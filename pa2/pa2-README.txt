Documentation for Warmup Assignment 2
=====================================

+------------------------+
| BUILD & RUN (Required) |
+------------------------+

Replace "(Comments?)" with either nunki.usc.edu, VMware, VirtualBox, or Vagrant Box:
    The grader should grade your submission on: VirtualBox
Replace "(Comments?)" with either Solaris (for nunki) or Ubuntu 14.04 or Ubuntu 16.04:
    I would prefer to be graded on: Ubuntu 16.04
If grading needs to be done on Ubuntu, replace "(Comments?)" with the names of the
packages your program depends on (to be used in "sudo apt-get install PACKAGENAME"):
    I need the following packages to be installed: tcsh, libssl-dev
Replace "(Comments?)" with the command the grader should use to compile your code (could simply be "make" or "gmake").
    To compile your code, the grader should type: make

+--------------+
| SELF-GRADING |
+--------------+

Replace each "?" below with a numeric value:

(A) Get Entire File : 25  out of 25 pts
(B) Get File Starting At An Offset : 25  out of 25 pts
(C) Get File Of Various Lengths : 25  out of 25 pts
(D) Get File With Offset And Lengths : 25  out of 25 pts

Missing/incomplete required section(s) in README file : -0 pts
Submitted binary file : -0 pts
Cannot compile : -0 pts
Compiler warnings   : -0 pts
"make clean" : -0 pts
Segmentation faults : -0 pts
Web client program never terminates : -0 pts
Malformed input : -0 pts
Bad commandline or command : -0 pts

+----------------------+
| BUGS / TESTS TO SKIP |
+----------------------+

Are there are any tests mentioned in the grading guidelines test suite that you
know that it's not working and you don't want the grader to run it at all so you
won't get extra deductions, please replace "(Comments?)" below with your list.
(Of course, if the grader won't run such tests in the plus points section, you
will not get plus points for them; if the garder won't run such tests in the
minus points section, you will lose all the points there.)  If there's nothing
the grader should skip, please replace "(Comments?)" with "none".

Please skip the following tests: (Comments?)

+-----------------------------------------------+
| OTHER (Optional) - Not considered for grading |
+-----------------------------------------------+

Comments on design decisions: (Comments?)

