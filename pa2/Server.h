//
//  Server.hpp
//  pa2
//
//  Created by WangBingtang on 1/25/18.
//  Copyright © 2018 WangBingtang. All rights reserved.
//

#ifndef Server_h
#define Server_h
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>
#include <fstream>

using namespace std;

class Server {
public:
    Server(int port,
           string logFileName, 
           string pidFileName,
           string filePathRoot, 
           string mimeFilePath);
    ~Server();
    bool readMimeFile();
    string getTypeByExt(string ext);
    vector<string> parseHTTPHeader(string buf);
    string processFileWithOffset(string filePath, string offset, string len, int& error);
    void writeIntoLogFile(string httpHeader, string address, int port, string localAddress, int localPort);
    string parseURL(string URL);
    void writeServerInfo(string addr, int port);

    string response();
    void resetInner();

private:
    // private member variable

    string filePathRoot; // TODO
    string pidFileName;
    string mimeFilePath;
    string logFileName;
    int port;

    char* fileContent;


    unordered_map<string, string> mimeMap;
    ofstream logFile;

    int getFileSize(ifstream& file);
    
    string fileType;
    long length;
    string fileMD5;
    int errorCode = 200;
    
    
};

#endif /* Server_h */
