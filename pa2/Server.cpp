//
//  Server.cpp
//  pa2
//
//  Created by WangBingtang on 1/25/18.
//  Copyright © 2018 WangBingtang. All rights reserved.
//

#include "Server.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <openssl/md5.h>
#include <stdio.h>
#include <iomanip>
#include <unistd.h>
#include <algorithm>
#include <climits>

const string WHILE_SPACE = "\t ";

/* Begin code (derived) from [https://stackoverflow.com/questions/236129/the-most-elegant-way-to-iterate-the-words-of-a-string] */
/* If the code you got requires you to include its copyright information, put copyright here. */
vector<string> splitString(string str, char delimiter){
    std::vector<std::string> tokens;
    std::size_t start = 0, end = 0;
    while ((end = str.find(delimiter, start)) != std::string::npos) {
        if (end != start) {
            tokens.push_back(str.substr(start, end - start));
        }
        start = end + 1;
    }
    if (end != start) {
        tokens.push_back(str.substr(start));
    }
    return tokens;
}
/* End code from [https://stackoverflow.com/questions/236129/the-most-elegant-way-to-iterate-the-words-of-a-string] */



// string Error405(){
//     string first = "\tHTTP/1.1 405 Method Not Allowed\r\n";
//     string second = "\tConnection: close\r\n";
//     string third = "\tContent-Type: text/html\r\n";
//     string fourth = "\tContent-Length: 72\r\n";
//     string fifth = "\t\r\n";
//     string sixth = "\t<html><head></head><body><h1>405 Method Not Allowed</h1></body></html>\r\n";
//     return  first + second + third + fourth + fifth + sixth;
// }

// string Error400(){
//     string first = "\tHTTP/1.1 400 Bad Request\r\n";
//     string second = "\tConnection: close\r\n";
//     string third = "\tContent-Type: text/html\r\n";
//     string fourth = "\tContent-Length: 65\r\n";
//     string fifth = "\t\r\n";
//     string sixth = "\t<html><head></head><body><h1>400 Bad Request</h1></body></html>\r\n";
//     return  first + second + third + fourth + fifth + sixth;
// }

// string Error404(){
//     string first = "\tHTTP/1.1 404 Not Found\r\n";
//     string second = "\tConnection: close\r\n";
//     string third = "\tContent-Type: text/html\r\n";
//     string fourth = "\tContent-Length: 63\r\n";
//     string fifth = "\t\r\n";
//     string sixth = "\t<html><head></head><body><h1>404 Not Found</h1></body></html>\r\n";
//     return  first + second + third + fourth + fifth + sixth;
// }

string Error405(){
    return "\t(ERROR) 405 Method Not Allowed\n\t(DONE)\n";
}
string Error404(){
    return "\t(ERROR) 404 Not Found\n\t(DONE)\n";
}
string Error400(){
    return "\t(ERROR) 400 Bad Request\n\t(DONE)\n";
}
/*
*   Definition of Class function
*/

Server::Server(int port,
           string logFileName, 
           string pidFileName,
           string filePathRoot, 
           string mimeFilePath)
{
    this->port = port;
    this->logFileName = logFileName;
    this->pidFileName = pidFileName;
    this->filePathRoot = filePathRoot;
    this->mimeFilePath = mimeFilePath;

    ofstream pidFile(filePathRoot + pidFileName);
    pidFile << (int)getpid() << endl;
    pidFile.close();

    logFile.open(logFileName, ios::out | ios::binary);
    cout << filePathRoot + logFileName << endl;
}

bool Server::readMimeFile(){
    if(mimeFilePath[0] != '/'){
        char buf[80];
        mimeFilePath = string(getcwd(buf, sizeof(buf))) + "/" + mimeFilePath;
    }
    
    ifstream mimeFile(mimeFilePath);
    //ifstream mimeFile(filePathRoot + mimeFilePath);
    if(!mimeFile) return false; // mime file doesn't exist
    
    // read the file and deal with each line
    string line;
    int lineNum = 0;
    while(getline(mimeFile, line)){
        lineNum++;
        if(line.empty())
            continue; // empty line
        // find the first non space (tab) character
        size_t found = line.find_first_not_of(WHILE_SPACE);
        if(found == string::npos || line[found] == '#')
            continue; // empty line or comment line
        
        stringstream ss(line); // remove leading and trailing space
        string firstField, secondField;
        ss >> firstField >> secondField;
        
        /*
         * type=text/html exts=htm,html
         * firstField = text/html;
         * secondField = htm,html
         */
        // check the first field
        vector<string> firstFieldVec = splitString(firstField, '=');
        if(firstFieldVec.size() != 2){
            cerr << "Mime file malformed at line " << lineNum 
                 << ": The first field should be 'type=type/subtype'\n";
            return false;
        }
        string type = splitString(firstField, '=')[1]; //text/html
        if(splitString(firstField, '=')[0] != "type"){
            cerr << "Mime file malformed at line " << lineNum 
                 << ": The first field should be 'type=type/subtype'\n";
            return false;
        }
        if(type.find("/") == string::npos){
            cerr << "Mime file malformed at line " << lineNum
                 << ": The first filed should be 'type=type/subtype'\n";
            return false;
        }
        vector<string> fileExtVec = splitString(secondField, '=');
        if(fileExtVec.size() != 2){
            cerr << "Mime file malformed at line " << lineNum
                 << ": The file extension should not be empty\n";
            return false;
        }
        if(fileExtVec[1].find_first_not_of(WHILE_SPACE) == string::npos){
            cerr << "Mime file malformed at line " << lineNum
                 << ": The file extension should not be empty\n";
            return false;
        }

        vector<string> fileExt = splitString(splitString(secondField, '=')[1], ',');
        for(string ext: fileExt){
            if(mimeMap.find(ext) == mimeMap.end()){
                mimeMap.insert(make_pair(ext, type));
            }else{
                cerr <<"Mime file malformed at line " << lineNum <<
                 ": One file extension cannot map to two type\n";
                return false;
            }
            
        }
    }
    
    return true;
}

string Server::getTypeByExt(string ext){
    transform(ext.begin(), ext.end(), ext.begin(), ::tolower);
    unordered_map<string, string>::iterator it = mimeMap.find(ext);
    if(it == mimeMap.end()){
        return ""; // extension not found
    } 
    return it->second;
}

vector<string> Server::parseHTTPHeader(string buf){
    vector<string> lines;
    lines = splitString(buf, '\n');
    for(unsigned int i = 0; i < lines.size(); i++){
        lines[i] = "\t" + lines[i] + "\n";
    }
    lines.push_back("\t\r\n");
    return lines;
}

string Server::processFileWithOffset(string filePath, string offset, string length, int& error){
    
    
    int fileLength = 0;
    string requestFile = "";
    string requestOff = "";
    string requestLen = "";
    string filePath2 = this->filePathRoot + filePath; //shift the path to default directory

    // get the file name
    vector<string> request = splitString(filePath2, '/');
    requestFile = request[request.size() - 1];
    // get file type
    vector<string> comma = splitString(requestFile, '.');
    string fileExt = comma[comma.size() - 1];
    this->fileType = getTypeByExt(fileExt);
    if(this->fileType == "") this->fileType = "application/octet-stream";

    requestFile = "\tRequest file: '" + filePath.substr(1) + "'\n";
    if(offset != ""){
        requestOff = "\tRequest off: " + offset + "\n";
    }
    if(length != ""){
        requestLen = "\tRequest len: " + length + "\n";
    }
    
    ifstream file;
    file.open(filePath2, ios::in);
    
    logFile << requestFile << requestOff << requestLen;
    logFile.flush();
    // check error
    if(!file){ // 404 not found
        error = 404;
        return "";
    }
    fileLength = getFileSize(file);

    int off = atoi(offset.c_str());
    int len;
    if(off > fileLength){ // 404 not found
        error = 404;
        return "";
    }  
    
    if(length == "") len = fileLength;
    else len = atoi(length.c_str());
    
    if(off + len > fileLength) len = fileLength - off;

    // if(off != 0){
    //     this->fileType = "application/octet-stream";
    // }
    // else if(len != fileLength){
    //     this->fileType = "application/octet-stream";
    // }
    if(len != fileLength){
        cout << fileExt << endl;
        this->fileType = "application/octet-stream";
    }

    this->length = len;
    // get file content
    file.seekg(off, ios::beg);
    this->fileContent = new char[len];
    file.read(this->fileContent, len);

    // for(int i = 0; i < len; i++){
    //     cout << this->fileContent[i];
    // }
    
    file.close();
    
    
    


    // get file MD5
    unsigned char c[MD5_DIGEST_LENGTH];
    MD5_CTX mdContext;
    FILE *inFile = fopen(filePath2.c_str(), "rb");
    cout << off << endl;
    fseek(inFile, off, SEEK_SET);
    
    
    int bytes;
    unsigned char* data = new unsigned char[len];
    MD5_Init(&mdContext);
    
    if ((bytes = fread (data, 1, len, inFile)) != 0)
        MD5_Update (&mdContext, data, bytes);
    MD5_Final (c,&mdContext);
    fclose (inFile);
    delete data;

    stringstream s;
    for(int i = 0; i < 16; ++i){
        s << std::hex << std::setfill('0') << std::setw(2) << (unsigned short)c[i];
    }
    this->fileMD5 = s.str();
    

    
    string fileTypeTemp = "\tContent-Type: " + this->fileType + "\n";
    string contentLength = "\tContent-Length: " + to_string(len) + "\n";
    string fileMD5Temp = "\tContent-MD5: " + this->fileMD5 + "\n";
    
    
    string result = fileTypeTemp + contentLength + fileMD5Temp; 
    file.close();
    return result;
}


// private member function
int Server::getFileSize(ifstream& file){
    streampos fsize = file.tellg();
    file.seekg(0, ios::end);
    fsize = file.tellg() - fsize;
    return fsize;
}

void Server::writeIntoLogFile(string httpHeader, string address, int port, string localAddress, int localPort){
    //logFile << endl;
    logFile << "Client connecting from " << address << ":" << port << endl;
    vector<string> lines = parseHTTPHeader(httpHeader);
    for(string line: lines){
        logFile << line;
    }

    // Parse URL
    logFile << parseURL(lines[0]);
    logFile << "\t(DONE)" << endl << endl;
    logFile.flush();
}

string Server::parseURL(string request){
    
    // GET /image.html?off=x&len=y HTTP/1.0\r\n
    stringstream ss(request);
    string method; // request method
    string URL; // request file path
    ss >> method >> URL;
    if(method != "GET"){
        this->errorCode = 405;
        return Error405();
    } 
    
    // validate the URL
    if(URL.substr(0, 3) == "../"){
        this->errorCode = 404;
        return Error404();
    } 
    if(URL.find("/../") != string::npos){
        this->errorCode = 404;
        return Error404();
    }

    string x = "", y = "";
    // image.html?off=x&len=y
    vector<string> questionMark = splitString(URL, '?');
    if(questionMark.size() >= 2){
        // parse off and len
        vector<string> andSign = splitString(questionMark[questionMark.size()-1], '&');
        for(size_t i = 0; i < andSign.size(); i++){
            string field = andSign[i];
            if(field.substr(0,3) == "off"){
                vector<string> offset = splitString(andSign[i], '=');
                if(offset.size() != 2){
                    this->errorCode = 400;
                    return Error400();
                }
                x = offset[1];
            }else if(field.substr(0,3) == "len"){
                vector<string> offset = splitString(andSign[i], '=');
                if(offset.size() != 2){
                    this->errorCode = 400;
                    return Error400();
                }
                y = offset[1];
            }else{
                this->errorCode = 400;
                return Error400();
            }
        }
    }

    // int x = -50, y = -50;
    // vector<string> questionMark = splitString(URL, '?');
    // if(questionMark.size() >= 2){
    //     // has ? and off and len
    //     vector<string> andSign = splitString(questionMark[questionMark.size() - 1], '&');
    //     if(andSign.size() > 2) {
    //         this->errorCode = 400;
    //         return Error400();
    //     }else if(andSign.size() == 1){
    //         vector<string> offset = splitString(andSign[0], '=');
    //         if(offset.size() != 2 || offset[0] != "off"){
    //             this->errorCode = 400;
    //             return Error400();
    //         }
    //         stringstream s1(offset[1]);
    //         s1 >> x;
    //     }else if(andSign.size() == 2){
    //         vector<string> offset = splitString(andSign[0], '=');
    //         vector<string> length = splitString(andSign[1], '=');
    //         if(offset.size() != 2 || offset[0] != "off" || 
    //            length.size() != 2 || length[0] != "len"){
    //                this->errorCode = 400;
    //                return Error400();
    //         }
    //         stringstream s1(offset[1]);
    //         s1 >> x; 
    //         stringstream s2(length[1]);
    //         s2 >> y;  
    //     }
        
        
    // }
    
    // TODO- read file and MD5
    int error = 200;
    if(x.find_first_not_of("0123456789") != std::string::npos ||
       y.find_first_not_of("0123456789") != std::string::npos){
           this->errorCode = 400;
            return Error400();
    }


    string content = processFileWithOffset(questionMark[0], x, y, error);
    this->errorCode = error;
    if(error == 404) return Error404();
    if(error == 405) return Error405();
    if(error == 400) return Error400();

    

    return content;
}

void Server::writeServerInfo(string addr, int port){
    logFile << "Server listening at " << addr << ":" << port<< endl;
    if(this->filePathRoot[this->filePathRoot.length()-1] == '/'){
        logFile << "Server root at '" << this->filePathRoot.substr(0, this->filePathRoot.length()-1) << "'\n";
    }else
        logFile << "Server root at '" << this->filePathRoot << "'\n";
    logFile << endl;
    logFile.flush();
}

string Server::response(){
    if(this->errorCode == 200){
        string line1 = "HTTP/1.1 200 OK\r\n";
        string line2 = "Connection: close\r\n";
        string line3 = "Server: pa2 bingtanw@usc.edu\r\n";
        string line4 = "Content-Type: " + this->fileType + "\r\n";
        string line5 = "Content-Length: " + to_string(this->length) + "\r\n";
        string line6 = "Content-MD5: " + this->fileMD5 + "\r\n";
        string line7 = "\r\n";
        string line8;
        line8.assign(this->fileContent, this->length);
        return line1 + line2 + line3 + line4 + line5 + line6 + line7 + line8;
    }
    else if(this->errorCode == 400){
        string first = "HTTP/1.1 400 Bad Request\r\n";
        string second = "Connection: close\r\n";
        string third = "Content-Type: text/html\r\n";
        string fourth = "Content-Length: 65\r\n";
        string fifth = "\r\n";
        string sixth = "<html><head></head><body><h1>400 Bad Request</h1></body></html>\r\n";
        return  first + second + third + fourth + fifth + sixth;
    }
    else if(this->errorCode == 404){
        string first = "HTTP/1.1 404 Not Found\r\n";
        string second = "Connection: close\r\n";
        string third = "Content-Type: text/html\r\n";
        string fourth = "Content-Length: 63\r\n";
        string fifth = "\r\n";
        string sixth = "<html><head></head><body><h1>404 Not Found</h1></body></html>\r\n";
        return  first + second + third + fourth + fifth + sixth;
    }
    else if(this->errorCode == 405){
        string first = "HTTP/1.1 405 Method Not Allowed\r\n";
        string second = "Connection: close\r\n";
        string third = "Content-Type: text/html\r\n";
        string fourth = "Content-Length: 72\r\n";
        string fifth = "\r\n";
        string sixth = "<html><head></head><body><h1>405 Method Not Allowed</h1></body></html>\r\n";
        return  first + second + third + fourth + fifth + sixth;
    }
    cout << "Error: " << this->errorCode << endl;
   return "";
}

void Server::resetInner(){
    //delete fileContent;
    fileContent = nullptr;
    this->fileType = "";
    this->length = 0;
    string fileMD5 = "";
    this->errorCode = 200;
}

Server::~Server(){
    
    logFile.close();
}