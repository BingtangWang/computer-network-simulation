//
//  main.cpp
//  pa2
//
//  Created by WangBingtang on 1/25/18.
//  Copyright © 2018 WangBingtang. All rights reserved.
//

#include <iostream>
#include <boost/asio.hpp>
#include <glog/logging.h>
#include "Server.h"

using namespace std;
using namespace boost::asio;

char buf[80];


DEFINE_int32(
    port, 5380,
    "Port number for server / where server is running");
DEFINE_string(
    log, "pa2.log",
    "log file to store http information");
DEFINE_string(
    pid, "pa2.pid",
    "Process ID file");
DEFINE_string(
    root, string(getcwd(buf, sizeof(buf))),
    "The root directory");

// value used as delimiter / for marking the end of a message
const std::string kDelimiter = "\r\n\r\n";

/*
*   Header of function
*/
void runServer(Server& s);
string readUntilDelimiter(
    boost::asio::ip::tcp::socket& socket,
    boost::asio::streambuf& readBuffer);
void sendWithDelimiter(
    boost::asio::ip::tcp::socket& socket,
    const std::string& message);
void trick(Server& s);
bool validateCommandLine(int argc, char *argv[]);
vector<string> splitString2(string str, char delimiter){
    std::vector<std::string> tokens;
    std::size_t start = 0, end = 0;
    while ((end = str.find(delimiter, start)) != std::string::npos) {
        if (end != start) {
            tokens.push_back(str.substr(start, end - start));
        }
        start = end + 1;
    }
    if (end != start) {
        tokens.push_back(str.substr(start));
    }
    return tokens;
}


int main(int argc, char *argv[]) {
    if(!validateCommandLine(argc, argv)){
        return 0;
    }

    // setup Google logging and flags
    // set logtostderr to true by default, but it can still be overridden by flags
    FLAGS_logtostderr = true;
    google::InitGoogleLogging(argv[0]);
    google::ParseCommandLineFlags(&argc, &argv, true);
    LOG(INFO) << "Started pa2";
    

    // verify port number set
    if (FLAGS_port == 0) {
        LOG(FATAL) << "Port number must be set";
    } else if (FLAGS_port > 65535 || FLAGS_port < 0) {
        LOG(FATAL) << "Invalid port number";
    }

    if(FLAGS_root[0] != '/'){
        FLAGS_root = string(getcwd(buf, sizeof(buf))) + "/" + FLAGS_root;
    }
    FLAGS_root += "/";
    if(FLAGS_log[0] == '/'){
        ofstream test(FLAGS_log);
        if(!test.is_open()){
            cerr << "Cannot open " << FLAGS_log << endl;
            return 0;
        }
    }
    

    Server s(FLAGS_port, FLAGS_log, FLAGS_pid, FLAGS_root, argv[argc-1]);
    if(!s.readMimeFile()){
        cerr << "Cannot read mime file\n";
        return 0;
     }

    runServer(s);

    return 0;
}


// Function: Run Server
void runServer(Server& s){
    trick(s);

    io_service ioService;
    ip::tcp::acceptor acceptor(
        ioService, ip::tcp::endpoint(ip::tcp::v4(), FLAGS_port));


    for (;;) {
        // setup a socket and wait on a client connection
        ip::tcp::socket socket(ioService);
        LOG(INFO) << "Waiting for client to connect";
        acceptor.accept(socket);

        
        // log the address of the remote client
        const auto remoteEndpoint = socket.remote_endpoint();
        const auto localEndpoint = socket.local_endpoint();
        LOG(INFO)
            << "Connected to client ("
            << remoteEndpoint.address() << ":" << remoteEndpoint.port() << ")";

        // wait for a message from the client
        LOG(INFO) << "Waiting for message from client";
        boost::asio::streambuf rcvBuffer;
        const auto message = readUntilDelimiter(socket, rcvBuffer);
        
        // TODO: message is the HTTP header, process this
        // GET /image.html HTTP/1.0
        // Host: localhost:5050
        // Accept: text/html, text/plain, text/sgml, text/css, */*;q=0.01
        // Accept-Encoding: gzip, compress, bzip2
        // Accept-Language: en
        // User-Agent: Lynx/2.8.9dev.8 libwww-FM/2.14 SSL-MM/1.4.1 GNUTLS/3.4.9
        // I0127 15:42:39.151803  7138 main.cpp:91] Sent message "GET /image.html HTTP/1.0
        // Host: localhost:5050
        // Accept: text/html, text/plain, text/sgml, text/css, */*;q=0.01
        // Accept-Encoding: gzip, compress, bzip2
        // Accept-Language: en
        // User-Agent: Lynx/2.8.9dev.8 libwww-FM/2.14 SSL-MM/1.4.1 GNUTLS/3.4.9"
        
        string address = remoteEndpoint.address().to_string();
        string localAddress = localEndpoint.address().to_string();

        s.writeIntoLogFile(message, address, remoteEndpoint.port(), localAddress, localEndpoint.port());
        



        const auto responseMessage = s.response();
        sendWithDelimiter(socket, responseMessage);
        s.resetInner();
        //LOG(INFO) << "Sent message \"" << responseMessage << "\"";

        // we're done
        LOG(INFO) << "Disconnected client";

        
    }
}

// Function: Read until delimiter
string readUntilDelimiter(boost::asio::ip::tcp::socket& socket, 
                          boost::asio::streambuf& readBuffer) 
{
    // blocking read on the socket until the delimiter
    boost::system::error_code error;
    const auto bytesTransferred = read_until(
        socket, readBuffer, kDelimiter, error);
    if (error) {
        LOG(FATAL)
            << "Read error: "
            << boost::system::system_error(error).what();
    }

    // read_until may read more data into the buffer (past our delimiter)
    // so we need to extract based on the bytesTransferred value
    const string readString(
        buffers_begin(readBuffer.data()),
        buffers_begin(readBuffer.data()) +
        bytesTransferred - kDelimiter.length());
    // consume all of the data (including the delimiter) from our read buffer
    readBuffer.consume(bytesTransferred);
    
    return readString;
}

void sendWithDelimiter(
    boost::asio::ip::tcp::socket& socket,
    const std::string& message) {
  boost::system::error_code error;
  const std::string messageWithDelimiter = message + kDelimiter;
  
  write(socket, buffer(messageWithDelimiter), transfer_all(), error);
  if (error) {
    LOG(FATAL)
        << "Send error: "
        << boost::system::system_error(error).what();
  }
}

void trick(Server& s){
    char server_name[256];
    int reuse_addr = 1;
    int master_socket_fd;
    struct addrinfo hints;
    struct addrinfo* res;

    gethostname(server_name, sizeof(server_name));
    
    

    memset(&hints,0,sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = 0;
    hints.ai_flags = AI_NUMERICSERV|AI_ADDRCONFIG;

    getaddrinfo(server_name, to_string(FLAGS_port).c_str(), &hints, &res);
    master_socket_fd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    setsockopt(master_socket_fd, SOL_SOCKET, SO_REUSEADDR, (void*)(&reuse_addr), sizeof(int));
    bind(master_socket_fd, res->ai_addr, res->ai_addrlen);
    listen(master_socket_fd, 1);
    {
        /* the following will show you exactly where your server thinks it's running at */
        struct sockaddr_in server_addr;
        socklen_t server_len = (socklen_t)sizeof(server_addr);

        getsockname(master_socket_fd, (struct sockaddr *)(&server_addr), &server_len);
        fprintf(stderr,
                "[SERVER] Server listening at %s:%1d\n",
                inet_ntoa(server_addr.sin_addr),
                (int)htons((uint16_t)(server_addr.sin_port & 0x0ffff)));
        // write into log file
        s.writeServerInfo(inet_ntoa(server_addr.sin_addr), (int)htons((uint16_t)(server_addr.sin_port & 0x0ffff)));
        
    }
    close(master_socket_fd);
}

bool validateCommandLine(int argc, char *argv[]){
    if(argc < 2){
        cerr << "Command line malformed: too few arguments\n";
        cerr << "Usage: pa2 [-port=number] [-log=logfilename] [-pid=pidfilename] [-root=directory] mimetypesfile\n";
        return false;
    }
    if(argc == 2){
        if(string(argv[1])[0] == '-'){
            cerr << "Command line malformed: must have mime file\n";
            cerr << "Usage: pa2 [-port=number] [-log=logfilename] [-pid=pidfilename] [-root=directory] mimetypesfile\n";
            return false;
        }
    }
    // check optional field
    for(int i = 1; i < argc - 1; i++){
        string field = string(argv[i]);
        if(field[0] != '-'){
            cerr << "Command line malformed: optional argument must have '-'\n";
            cerr << "Usage: pa2 [-port=number] [-log=logfilename] [-pid=pidfilename] [-root=directory] mimetypesfile\n";
            return false;
        }
        
        if(field.substr(1, 4) == "port" || field.substr(1, 3) == "log" ||
           field.substr(1, 3) == "pid" || field.substr(1, 4) == "root"){
            
        }else{
            cout << field.substr(1, 4) << endl;
            cerr << "Command line malformed: invalid optional argument\n";
            cerr << "Usage: pa2 [-port=number] [-log=logfilename] [-pid=pidfilename] [-root=directory] mimetypesfile\n";
            return false;
        }

        vector<string> split = splitString2(field, '=');
        if(split.size() != 2){
            cerr << "Command line malformed: equal sign no content\n";
            cerr << "Usage: pa2 [-port=number] [-log=logfilename] [-pid=pidfilename] [-root=directory] mimetypesfile\n";
            return false;
        }
        size_t found = split[1].find_first_not_of("\t ");
        if(found == string::npos){
            cerr << "Command line malformed: equal sign no content\n";
            cerr << "Usage: pa2 [-port=number] [-log=logfilename] [-pid=pidfilename] [-root=directory] mimetypesfile\n";
            return false;
        }
        if(field.substr(1, 4) == "port" && 
            split[1].find_first_not_of("0123456789") != std::string::npos){
            cerr << "Command line malformed: port must be all digits\n";
            cerr << "Usage: pa2 [-port=number] [-log=logfilename] [-pid=pidfilename] [-root=directory] mimetypesfile\n";
            return false;
        }

    }
    return true;
}
