#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <openssl/md5.h>
#include <iomanip>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <glog/logging.h>
#include <vector>
#include <fstream>
#include <sstream>
#include <ctime>
#include <unordered_map>
#include <time.h>
#include <math.h>

using namespace std;

DEFINE_string(
    out, "",
    "the file that download from the sever"
);
DEFINE_string(
    log, "pa3.log",
    "The log file"
);
DEFINE_string(
    mailcap, "",
    "The mailcap file"
);
void printUsage(){
    cerr << "pa3 -out=filename [-log=logfilename] [-mailcap=mailcapfile] URL" << endl;
}

/*
*   Header of Function
*/
vector<string> splitString(string str, char delimiter);
bool parseURL(string url, string& hostname, int& port, string& everyElse);
string httpRequestHeader(string everyElse, string hostname, int port);
string timeStamp();
string computeMD5(string out, long long len);
bool parseMailcap(string filePath, unordered_map<string, string>& mailcapMap);
string getCommandByType(string type, unordered_map<string, string>& mailcapMap);
bool validateCommandLine(int argc, char *argv[]);
void mailCapOpen(unordered_map<string, string> mailcapMap, string outFileName, string type);
void showProgressBar(long long byteCount, long long contentLength, double elapsedTime);
/*
*   Main()
*/
int main(int argc, char *argv[])
{
    // test field
    
    // end test field
    if(!validateCommandLine(argc, argv)){
        return 0;
    }
    // parse the command line
    // setup Google logging and flags
    // set logtostderr to true by default, but it can still be overridden by flags
    FLAGS_logtostderr = true;
    google::InitGoogleLogging(argv[0]);
    google::ParseCommandLineFlags(&argc, &argv, true);

    // check if log can be opened;
    ofstream checkLog(FLAGS_log);
    if(!checkLog.is_open()){
        cerr << "Cannot open " << FLAGS_log << endl;
        return 0;
    }
    unordered_map<string, string> mailcapMap;
    if(FLAGS_mailcap != "" && !parseMailcap(FLAGS_mailcap, mailcapMap)){
        cerr << "Cannot read mailcap" << endl;
        return 0;
    }
    
    LOG(INFO) << "Started pa3";
    
    
    if(FLAGS_out == ""){
        // must have stuff in out
        cerr << "CommandLine malformed: must have outfile input\n";
        printUsage();
        return 0;
    }
    string url = argv[argc - 1];
    string hostname, everyElse;
    

    int port;
    if(!parseURL(url, hostname, port, everyElse)){
        printUsage();
        return 0;
    }
    // open log file
    ofstream logFile(FLAGS_log);
    ofstream outFile(FLAGS_out);

    // Build up client socket
    char server_name[256], buf[1];
    int client_socket_fd;
    struct addrinfo hints;
    struct addrinfo* res;
    struct sockaddr_in soc_address;
    int bytes_received;
    int server_port;

    strncpy(server_name, hostname.c_str(), sizeof(hostname));
    server_port = port;
    

    memset(&hints,0,sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = 0;
    hints.ai_flags = AI_NUMERICSERV|AI_ADDRCONFIG;
    
    getaddrinfo(server_name, to_string(port).c_str(), &hints, &res);

    client_socket_fd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    memset(&soc_address, 0, sizeof(soc_address));
    if (*server_name >= '0' && *server_name <= '9') {
        soc_address.sin_addr.s_addr = inet_addr(server_name);
    } else {
        struct hostent *p_hostent;

        p_hostent = gethostbyname(server_name);
        memcpy(&soc_address.sin_addr, p_hostent->h_addr, p_hostent->h_length);
    }
    soc_address.sin_family = AF_INET;
    soc_address.sin_port = htons((unsigned short)server_port);
    connect(client_socket_fd, (struct sockaddr*)&soc_address, sizeof(soc_address));
    {
        struct sockaddr_in cs_addr;
        socklen_t cs_len = (socklen_t)sizeof(cs_addr);

        /* the following will show you exactly where your client is running at */
        getsockname(client_socket_fd, (struct sockaddr *)(&cs_addr), &cs_len);
        fprintf(stderr,
                "[CLIENT] Client at %s:%1d\n",
                inet_ntoa(cs_addr.sin_addr),
                (int)htons((uint16_t)(cs_addr.sin_port & 0x0ffff)));
        getpeername(client_socket_fd, (struct sockaddr *)(&cs_addr), &cs_len);
        fprintf(stderr,
                "[CLIENT] Client thinks the server is at %s:%1d\n",
                inet_ntoa(cs_addr.sin_addr),
                (int)htons((uint16_t)(cs_addr.sin_port & 0x0ffff)));
        
         // write connection info into log file
        logFile << "Client at " << inet_ntoa(cs_addr.sin_addr) << ":" 
                << (int)htons((uint16_t)(cs_addr.sin_port & 0x0ffff)) << "\n"
                << "URL: " << url << "\n"
                << "Connected to server at " << inet_ntoa(cs_addr.sin_addr) << ":"
                << (int)htons((uint16_t)(cs_addr.sin_port & 0x0ffff)) << "\n";
        logFile.flush();
    }
   
    string header = httpRequestHeader(everyElse, hostname, port);
    char* mess = new char[header.length()];
    for(size_t i = 0; i < header.length(); i++){
        mess[i] = header[i];
    }
    
    // write stuff to server
    write(client_socket_fd, mess, strlen(mess));
    delete mess;
    // read response from server
    
    string line = "\t";
    string buffer = "";
    do{
        bytes_received = read(client_socket_fd, buf, sizeof(buf)); // read one byte at a time
        line += buf[0]; 
        buffer += buf[0];
        if(buf[0] == '\n'){ // reach the end of a line
            logFile << line;
            logFile.flush();
            if(line == "\t\r\n"){
                //logFile << line;
                // empty line
                break;
            }
            line = "\t";
        }
    } while(bytes_received > 0);

    // write reponse header into log file

        // check header
    if(buffer.substr(0, 5) != "HTTP/"){
        logFile << "\t[Abort] invalid HTTP response header\n";
        logFile.close();
        return 0;
    }
    if(buffer.find("Content-Length") == string::npos){
        logFile << "\t[Abort] invalid HTTP response header\n";
        logFile.close();
        return 0;
    }

    // get the content length
    size_t found = buffer.find("Content-Length");
    string sizeStr = "";
    for(size_t i = found + 16; i < buffer.length(); i++){
        if(buffer[i] == '\r') break;
        sizeStr += buffer[i];
    }
    long long contentLength = stoll(sizeStr); // the length of the content
    
    
    logFile << "\t[Date: " << timeStamp() << "] Start downloading " 
            << contentLength << " bytes\n";
    logFile.flush();

    // start downloading
    clock_t startTime = clock();
    clock_t lastTime = startTime, currTime;
    long long lenTemp = 0;
    do{
        bytes_received = read(client_socket_fd, buf, sizeof(buf)); // read one byte at a time
        outFile << buf[0];
        lenTemp++;
        // display progress
        currTime = clock();
        double timeTotal = (currTime - startTime)/(double)CLOCKS_PER_SEC;
        double timePassed = (currTime - lastTime)/(double)CLOCKS_PER_SEC;
        if(timePassed > 0.1){
            lastTime = currTime; // update lastTime
            showProgressBar(lenTemp, contentLength, timeTotal);
        }

    } while(lenTemp < contentLength);
    showProgressBar(lenTemp, contentLength, (currTime - startTime)/(double)CLOCKS_PER_SEC);
    cout << endl << endl;
    outFile.close();
    
    //check if content length == temp length
    if(contentLength != lenTemp){
        cout << "Length not equal\n";
        //\tFailed to read CONTENT_LENGTH bytes of data (BYTES_READ bytes read)\n
        //\t[Date: TIMESTAMP] Download aborted\n
        logFile << "\tFailed to read " << contentLength << " bytes of data (" << lenTemp << " bytes read)\n";
        logFile << "\t[Date: " << timeStamp() << "] Download aborted\n";
        return 0;
    }

    double time = (currTime - startTime)/(double)CLOCKS_PER_SEC;
    // download successfully
    
    if(time < 1){
        logFile << "\t[Date: " << timeStamp() << "] " << contentLength << " bytes downloaded in < 1s\n";
    }else{
        logFile << "\t[Date: " << timeStamp() << "] " << contentLength << " bytes downloaded in " << time << "s\n";
    }
    



    // check if buffer have content-MD5
    found = buffer.find("Content-MD5");
    string md5Str = "";
    if(found != string::npos){
        // has content-MD5
        for(size_t i = found + 13; buffer[i] != '\r' && buffer[i] != '\n'; i++){
            md5Str += buffer[i];
        }
    }
    if(md5Str != ""){
        // compare two md5
        string outMD5 = computeMD5(FLAGS_out, contentLength);
        if(outMD5 == md5Str){
            // TODO: output verified
            logFile << "\tContent-MD5: " << outMD5 << " (Verified)\n";
        }else{
            // TODO: output not verified
            logFile << "\tUnexpected MD5: " << outMD5 
                    << " (expecting: " << md5Str << ")\n";  
            unlink(FLAGS_out.c_str());
        }
        logFile.flush();
    }

    // find content type
    found = buffer.find("Content-Type");
    string type = "";
    if(found != string::npos){
        // has content-type
        for(size_t i = found + 14; buffer[i] != '\r' && buffer[i] != '\n'; i++){
            type += buffer[i];
        }
    }
    mailCapOpen(mailcapMap, FLAGS_out, type); // open the program
    write(1, buf, bytes_received);
    close(client_socket_fd);

    return 0;
}

/*
* Definition of function
*/
/* Begin code (derived) from [https://stackoverflow.com/questions/236129/the-most-elegant-way-to-iterate-the-words-of-a-string] */
/* If the code you got requires you to include its copyright information, put copyright here. */
vector<string> splitString(string str, char delimiter){
    std::vector<std::string> tokens;
    std::size_t start = 0, end = 0;
    while ((end = str.find(delimiter, start)) != std::string::npos) {
        if (end != start) {
            tokens.push_back(str.substr(start, end - start));
        }
        start = end + 1;
    }
    if (end != start) {
        tokens.push_back(str.substr(start));
    }
    return tokens;
}
/* End code from [https://stackoverflow.com/questions/236129/the-most-elegant-way-to-iterate-the-words-of-a-string] */


bool parseURL(string url, string& hostname, int& port, string& everyElse){
    if(url.substr(0, 7) != "http://"){
        cerr << "URL malformed" << endl;
        return false;
    }
    size_t i = 7;
    string hostnameTemp = "";
    string portTemp = "";
    bool host = true;
    while(i < url.length() && url[i] != '/'){
        if(url[i] == ':'){
            host = false;
            i++;
            continue;
        }

        if(host){
            hostnameTemp += url[i];
        }else{
            portTemp += url[i];
        }
        i++;
    }
    if(portTemp == "") portTemp = "80";
    // cout << hostnameTemp << endl;
    if(portTemp.find_first_not_of("0123456789") != string::npos){
        cerr << "URL malformed" << endl;
        return false;
    }
    
    hostname = hostnameTemp;
    stringstream ss(portTemp);
    ss >> port;
    if(i < url.size()){
        everyElse = url.substr(i+1);
    }
    // cout << everyElse << endl;
    
    return true;
}

string httpRequestHeader(string everyElse, string hostname, int port){
    // GET /EVERYTHINGELSE HTTP/1.0\r\n
    // Host: HOSTNAME:PORT\r\n
    // Accept: text/html, text/plain, text/sgml, */*\r\n
    // User-Agent: pa3 (YOURLOGIN)\r\n
    // \r\n
    string line1 = "GET /" + everyElse + " HTTP/1.0\r\n";
    string line2 = "Host: " + hostname + ":" + to_string(port) + "\r\n";
    string line3 = "Accept: text/html, text/plain, text/sgml, */*\r\n";
    string line4 = "User-Agent: pa3 (bingtanw)\r\n";
    string line5 = "\r\n";
    return line1 + line2 + line3 + line4 + line5;
}

string timeStamp(){
    // time_t rawtime;
    // struct tm * timeinfo;
    // char buffer [80];

    // time (&rawtime);
    // timeinfo = localtime (&rawtime);

    // strftime (buffer,80,"%a %b %e %Y %T.",timeinfo);
    // puts (buffer);

    timeval tim;
    gettimeofday(&tim, NULL);
    
    string time = ctime(&tim.tv_sec);
    time = time.substr(0, time.length()-1);
    
    return time.substr(0, 11) + time.substr(20, 4) + 
           " " + time.substr(11, 8) + "." + to_string(tim.tv_usec);
    // return string(buffer);
}

string computeMD5(string out, long long len){
    // get file MD5
    unsigned char c[MD5_DIGEST_LENGTH];
    MD5_CTX mdContext;
    FILE *inFile = fopen(out.c_str(), "rb");
    fseek(inFile, 0, SEEK_SET);
    int bytes;
    unsigned char* data = new unsigned char[len];
    MD5_Init(&mdContext);
    if ((bytes = fread (data, 1, len, inFile)) != 0)
        MD5_Update (&mdContext, data, bytes);
    MD5_Final (c,&mdContext);
    fclose (inFile);
    delete data;

    stringstream s;
    for(int i = 0; i < 16; ++i){
        s << std::hex << std::setfill('0') << std::setw(2) << (unsigned short)c[i];
    }
    return s.str();
}

bool parseMailcap(string filePath, unordered_map<string, string>& mailcapMap){
    if(filePath[0] != '/'){
        char buf[80];
        filePath = string(getcwd(buf, sizeof(buf))) + "/" + filePath;
    }
    ifstream mailcapFile(filePath);
    if(!mailcapFile){
        cerr << "Mailcap file doesn't exist\n";
        return false;
    } 

    string line;
    int lineNum = 0;
    while(getline(mailcapFile, line)){
        lineNum++;
        if(line.empty()){
            continue; // empty line
        }
        size_t found = line.find_first_not_of("\t ");
        if(found == string::npos || line[found] == '#')
            continue; // empty line or comment line

        vector<string> twoFields = splitString(line, ';');
        if(twoFields.size() != 2){
            cerr << "Mailcap file malformed at line " << lineNum
                 << ": each line must contain exactly two fields separated by a single semicolon character\n"; 
            return false;
        }
        // check the first field
        vector<string> types = splitString(twoFields[0], '/');
        if(types.size() != 2){
            cerr << "Mailcap file malformed at line " << lineNum
                 << ": The first field must be type/subtype\n"; 
            return false;
        }
        for(size_t i = 0; i < types.size(); i++){
            if(types[i].find_first_not_of("\t ") == string::npos){
                cerr << "Mailcap file malformed at line " << lineNum
                 << ": The first field must be type/subtype\n"; 
                 return false;
            }
        }

        // check the second field
        string field2 = twoFields[1];
        found = field2.find("'%s'");
        if(found == string::npos){
            cerr << "Mailcap file malformed at line " << lineNum
                 << ": The second field must have '%s'\n";
            return false;
        }else{
            found = field2.substr(found+4).find("'%s'");
            if(found != string::npos){
                cerr << "Mailcap file malformed at line " << lineNum
                     << ": The second field must have only one '%s'\n";
                return false;
            }
        }

        if(mailcapMap.find(twoFields[0]) == mailcapMap.end()){
            mailcapMap.insert(make_pair(twoFields[0], twoFields[1]));
        }else{
            cerr << "Mailcap file malformed at line " << lineNum
                 << ": The first field must only map to one command\n";
            return false;
        }


    }
    
    return true;
}

string getCommandByType(string type, unordered_map<string, string>& mailcapMap){
    unordered_map<string, string>::iterator it = mailcapMap.find(type);
    if(it == mailcapMap.end()) return ""; // extension not found
    return it->second;
}

bool validateCommandLine(int argc, char *argv[]){
    if(string(argv[argc-1])[0] == '-'){
        cerr << "Command line malformed: must have url\n";
        printUsage();
        return false;
    }
    if(argc < 2){
        cerr << "Command line malformed: too few arguments\n";
        printUsage();
        return false;
    }
    if(argc == 2){
        if(string(argv[1])[0] == '-'){
            cerr << "Command line malformed: must have url\n";
            printUsage();
            return false;
        }
    }
    // check optional field
    for(int i = 1; i < argc - 1; i++){
        string field = string(argv[i]);
        if(field[0] != '-'){
            cerr << "Command line malformed: optional argument must have '-'\n";
            printUsage();
            return false;
        }
        
        if(field.substr(1, 3) == "out" || field.substr(1, 3) == "log" ||
           field.substr(1, 7) == "mailcap"){
            
        }else{
            cerr << "Command line malformed: invalid optional argument\n";
            printUsage();
            return false;
        }

        vector<string> split = splitString(field, '=');
        if(split.size() != 2){
            cerr << "Command line malformed: equal sign no content\n";
            printUsage();
            return false;
        }
        size_t found = split[1].find_first_not_of("\t ");
        if(found == string::npos){
            cerr << "Command line malformed: equal sign no content\n";
            printUsage();
            return false;
        }
        if(field.substr(1, 4) == "port" && 
            split[1].find_first_not_of("0123456789") != std::string::npos){
            cerr << "Command line malformed: port must be all digits\n";
            printUsage();
            return false;
        }

    }
    return true;
}

void mailCapOpen(unordered_map<string, string> mailcapMap, 
                 string outFileName, 
                 string type)
{
    string mailcap = "";
    if(mailcapMap.find(type) != mailcapMap.end()){
        mailcap = mailcapMap.find(type)->second;
    }else{
        return;
    }
    
    char cmd[1024];
    FILE *pfp=NULL;

    snprintf(cmd, sizeof(cmd), mailcap.c_str(), outFileName.c_str());
    if ((pfp=(FILE*)popen(cmd,"r")) != NULL) {
        char buf[256];
        int bytes_read=0;

        if ((bytes_read=(int)fread(buf, sizeof(char), sizeof(buf), pfp)) > 0) {
            fwrite(buf, sizeof(char), bytes_read, stderr);
        }
        pclose(pfp);
        
    }
    
}

void showProgressBar(long long byteCount, long long contentLength, double elapsedTime){
    string BC = "";
    // process byte count to 3 digit
    if(byteCount < 1000){
        BC = to_string(byteCount) + " B";
    }else if(byteCount < 1000000){
        if(byteCount > 100000)
            BC = to_string((double)byteCount/1000.0).substr(0, 3) + " KB";
        else
            BC = to_string((double)byteCount/1000.0).substr(0, 4) + " KB";
    }else if(byteCount < 1000000000){
        if(byteCount > 100000000)
            BC = to_string((double)byteCount/1000000.0).substr(0, 3) + " MB";
        else
            BC = to_string((double)byteCount/1000000.0).substr(0, 4) + " MB";
    }else if(byteCount < 1000000000000){
        if(byteCount > 100000000000)
            BC = to_string((double)byteCount/1000000000.0).substr(0, 3) + " GB";
        else
            BC = to_string((double)byteCount/1000000000.0).substr(0, 4) + " GB";
    }else{
        BC = to_string((double)byteCount/1000000000.0).substr(0, 4) + " GB";
    }
    
    // content length
    string CL = "";
    if(contentLength < 1000){
        CL = to_string(contentLength) + " B";
    }else if(contentLength < 1000000){
        if(contentLength > 100000)
            CL = to_string((double)contentLength/1000.0).substr(0, 3) + " KB";
        else
            CL = to_string((double)contentLength/1000.0).substr(0, 4) + " KB";
    }else if(contentLength < 1000000000){
        if(contentLength > 100000000)
            CL = to_string((double)contentLength/1000000.0).substr(0, 3) + " MB";
        else
            CL = to_string((double)contentLength/1000000.0).substr(0, 4) + " MB";
    }else if(contentLength < 1000000000000){
        if(contentLength > 100000000000)
            CL = to_string((double)contentLength/1000000000.0).substr(0, 3) + " GB";
        else
            CL = to_string((double)contentLength/1000000000.0).substr(0, 4) + " GB";
    }else{
        CL = to_string((double)contentLength/1000000000.0).substr(0, 4) + " GB";
    }
    
    // percentage
    string P = "";
    double percent = (double)byteCount/contentLength * 100;
    if(percent < 1){
        P = "(< 1%)";
    }else if(percent < 100){
        P = "(" + to_string(percent).substr(0, 4) + "%)";
    }else{
        P = "(" + to_string(percent).substr(0, 3) + "%)";
    }
    

    // throughput
    string TP = "";
    double tp = (double)byteCount/elapsedTime;
    if(tp < 1){
        TP = "< 1 B/s";
    }else if(tp < 1000){
        TP = to_string(tp) + " B";
    }else if(tp < 1000000){
        if(tp > 100000)
            TP = to_string((double)tp/1000.0).substr(0, 3) + " KB/s";
        else
            TP = to_string((double)tp/1000.0).substr(0, 4) + " KB/s";
    }else if(tp < 1000000000){
        if(tp > 100000000)
            TP = to_string((double)tp/1000000.0).substr(0, 3) + " MB/s";
        else
            TP = to_string((double)tp/1000000.0).substr(0, 4) + " MB/s";
    }else if(tp < 1000000000000){
        if(tp > 100000000000)
            TP = to_string((double)tp/1000000000.0).substr(0, 3) + " GB/s";
        else
            TP = to_string((double)tp/1000000000.0).substr(0, 4) + " GB/s";
    }else{
        TP = to_string((double)tp/1000000000.0).substr(0, 4) + " GB/s";
    }
    
    // string time elapsed
    string ET = "";
    if(elapsedTime < 1){
        ET = "< 1s";
    }else if(elapsedTime < 60){
        ET = to_string(elapsedTime).substr(0, 4) + "s";
    }else if(elapsedTime < 3600){
        int min = elapsedTime/60;
        ET = to_string(min) + "min" + to_string(elapsedTime-min*60).substr(0, 4) + "s";
    }else if(elapsedTime < 86400){
        int hour = elapsedTime/3600;
        int min = (elapsedTime-hour*3600)/60;
        ET = to_string(hour) + "hr" + to_string(min) + "min" + to_string(elapsedTime-hour*3600-min*60).substr(0, 4) + "s";
    }else{
        int day = elapsedTime/86400;
        int hour = (elapsedTime-day*86400)/3600;
        int min = (elapsedTime-day*86400-hour*3600)/60;
        ET = to_string(day) + "day" + to_string(hour) + "hr" + 
             to_string(min) + "min" + to_string(elapsedTime-day*86400-hour*3600-min*60).substr(0, 4) + "s";

    }
    
    string result = BC + " of " + CL + " " + P + " downloaded at " + TP + ", time elapsed: " + ET;
    cerr << "\r";
    cerr << result << "   ";
}