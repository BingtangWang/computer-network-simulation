Documentation for Warmup Assignment 5
=====================================

+------------------------+
| BUILD & RUN (Required) |
+------------------------+

Replace "(Comments?)" with either nunki.usc.edu, VMware, VirtualBox, or VagrantBox:
    The grader should grade your submission on: VirtualBox

Replace "(Comments?)" with either Solaris (for nunki) or Ubuntu 14.04 or Ubuntu 16.04:
    I would prefer to be graded on: Ubuntu 16.04

If grading needs to be done on Ubuntu, replace "(Comments?)" with the names of the
packages your program depends on (to be used in "sudo apt-get install PACKAGENAME"):
    I need the following packages to be installed: tcsh, libssl-dev

Replace "(Comments?)" with the command the grader should use to compile your code (could simply be "make" or "gmake").
    To compile your code, the grader should type: make

Replace "(Comments?)" with the "host" the grader should use in the "gen-pa5-config.csh" file (one common answer for this is "127.0.0.1").
    To generate all the configuration files using "gen-pa5-config.csh", the grader should set host in "gen-pa5-config.csh" to: 127.0.1.1

+--------------+
| SELF-GRADING |
+--------------+

Replace each "?" below with a numeric value:

(A) Single famous node : 4 out of 4 points
(B) Two famous nodes : ? 13 of 13 points
(C) Three famous nodes : 12 out of 12 points
(D) One famous node and one regular node with num_startup_neighbors=1 : 13 out of 13 points
(E) One famous node and one regular node with num_startup_neighbors=2 : 8 out of 8 points
(F) One famous node and two regular nodes both with num_startup_neighbors=1, linear formation : 12.5 out of 12.5 points
(G) One famous node and two regular nodes both with num_startup_neighbors=1, linear formation : 13.5 out of 13.5 points
(H) One famous node and four regular nodes all with num_startup_neighbors=1, fork formation : 14 out of 14 points
(I) One famous node and three regular nodes with num_startup_neighbors=1 and one regular node with num_startup_neighbors=2 : 10 out of 10 points

Missing/incomplete required section(s) in README file : -0 pts
Submitted binary file : -0 pts
Cannot compile : -0 pts
Compiler warnings   : -0 pts
"make clean" : -0 pts
Segmentation faults : -0 pts
Cannot handle "log" key in [config] properly : -0 pts
Cannot handle "root" key in [config] properly : -0 pts
Node freezes up : -0 pts

+----------------------+
| BUGS / TESTS TO SKIP |
+----------------------+

Are there are any tests mentioned in the grading guidelines test suite that you
know that it's not working and you don't want the grader to run it at all so you
won't get extra deductions, please replace "(Comments?)" below with your list.
(Of course, if the grader won't run such tests in the plus points section, you
will not get plus points for them; if the garder won't run such tests in the
minus points section, you will lose all the points there.)  If there's nothing
the grader should skip, please replace "(Comments?)" with "none".

Please skip the following tests: (Comments?)

+-----------------------------------------------+
| OTHER (Optional) - Not considered for grading |
+-----------------------------------------------+

Comments on design decisions: (Comments?)

