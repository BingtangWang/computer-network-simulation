#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <thread>
#include <memory>
#include <mutex>
#include <atomic>
#include <fstream>
#include <unordered_map>
#include <vector>


#include <pthread.h>
#include "ClientConnection.h"

using namespace std;


const string WHILE_SPACE = "\t ";
/* Begin code (derived) from [https://stackoverflow.com/questions/236129/the-most-elegant-way-to-iterate-the-words-of-a-string] */
/* If the code you got requires you to include its copyright information, put copyright here. */
vector<string> splitString(string str, char delimiter){
    std::vector<std::string> tokens;
    std::size_t start = 0, end = 0;
    while ((end = str.find(delimiter, start)) != std::string::npos) {
        if (end != start) {
            tokens.push_back(str.substr(start, end - start));
        }
        start = end + 1;
    }
    if (end != start) {
        tokens.push_back(str.substr(start));
    }
    return tokens;
}
/* End code from [https://stackoverflow.com/questions/236129/the-most-elegant-way-to-iterate-the-words-of-a-string] */

/*
    Global variable (Thread Manager)
*/
atomic<int> _nextClientID (1);
ofstream log;
unordered_map<string, unordered_map<string, string>> config;
unordered_map<string, string> mimeMap;

string root;
vector<ClientConnection*> connectionManager(999999);
mutex clientConnectionMutex;

/*
    Header of functions
*/
void printConsoleUsage();
bool praseConfig(string path);
bool isDigit(string str, int& ret);
void writeServerInfo(string addr, int port);
string getConfigValue(string section, string key);
bool readMimeFile(string mimeFilePath);
bool validateCommandLine(int argc, char *argv[]);

// End of Header
void *handle_connection(void *arg)
{
    // testing field
    
    ClientConnection* cc = new ClientConnection(arg, _nextClientID, config, mimeMap, root);
    
    {
        lock_guard<mutex> guard(clientConnectionMutex);
        connectionManager[_nextClientID - 1] = cc;
        _nextClientID.fetch_add(1);
    }
    //cc->run();
    {
        lock_guard<mutex> guard(clientConnectionMutex);
        cc->writeInLog(log);
    }
    cc->response(log);
    
    
    connectionManager[cc->getClientID()-1] = nullptr;
    delete cc;
    return NULL;
}

void *console(void *arg)
{
    // char buf[1024];

    // printf("(This console echoes whatever you type.)\n");
    // printf("> ");
    // while (fgets(buf, sizeof(buf), stdin) != NULL) {
    //     printf("%s", buf);
    //     printf("> ");
    // }
    // printf("The console is closed.  (Should close all connections and shutdown immediately.)\n");
    
    while(true){
        cout << "> ";
        string command;
        getline(cin, command);
        vector<string> line = splitString(command, ' ');
        
        command = line[0];
        if(command == "info"){
            if(line.size() < 2){
                cerr << "Too less argument for command 'info'\n";
                printConsoleUsage();
                continue;
            }
            int argument;
            if(isDigit(line[1], argument) && argument > 0 && argument < 999999){
                // TO-DO: show connection information for connection[arguemnt]
                ClientConnection* cc = connectionManager[argument-1];
                if(cc == nullptr){
                    cerr << "No such connection: " << argument << "\n";
                }else{
                    if(cc->hasTerminated){
                        cerr << "No such connection: " << argument << "\n";
                        delete cc;
                        connectionManager[argument-1] = nullptr;
                    }else{
                        cc->printInfo();
                    }
                    
                }
                

            }else{
                cerr << "Argument for info commmand must be postivie integer" << endl;
                printConsoleUsage();
            }
        }else if(command == "status"){
            // TO-DO: show information for all connections
            bool hasConnection = false;
            for(size_t i = 0; i < connectionManager.size(); i++){
                if(connectionManager[i] != nullptr){
                    if(connectionManager[i]->hasTerminated){
                        delete connectionManager[i];
                        connectionManager[i] = nullptr;
                        continue;
                    }
                    connectionManager[i]->printInfo();
                    hasConnection = true;
                }
            }
            if(!hasConnection){
                cerr << "No active connection\n";
            }
        }else if(command == "dial"){
            int argument1, argument2;
            if(line.size() < 3){
                cerr << "Dial command must have 3 arguements" << endl;
                printConsoleUsage();
                continue;
            }
            if(isDigit(line[1], argument1) && isDigit(line[2], argument2)){
                if(argument2 > 100 || argument2 < 1){
                    cerr << "Dial value is out of range (it must be >=1 and <= 100)\n" << endl;
                    printConsoleUsage();
                    continue;
                }
                // TO-DO: change the value of the rate for connection [argument1]
                if(connectionManager[argument1-1] == nullptr || connectionManager[argument1-1]->hasTerminated){
                    cerr << "No such connection: " << argument1 << "\n";
                    continue;
                }
                connectionManager[argument1-1]->setDial(argument2, log);
                cerr << "Dial for connection " << argument1 << " changed to " << argument2 
                     << "%.  Token rate now at " << connectionManager[argument1-1]->getTokenRate() << " KB/s\n";

            }else{
                cerr << "Arguments for dial command must all be integers" << endl;
                printConsoleUsage();
                continue;
            }


        }else if(command == "close"){
            int argument;
            if(isDigit(line[1], argument) && argument > 0 && argument < 999999){
                // TO-DO: close the connection[argument]
                if(connectionManager[argument-1] == nullptr || connectionManager[argument-1]->hasTerminated){
                    cerr << "No such connection: " << argument << "\n";
                    continue;
                }
                pthread_t threadID = connectionManager[argument-1]->threadID;
                connectionManager[argument-1]->terminate(log);
                delete connectionManager[argument-1];
                connectionManager[argument-1] = nullptr;
                pthread_cancel(threadID);
                cerr << "Connection " << argument << " is closed\n";
            }else{
                cerr << "Argument for close commmand must be positive integer" << endl;
                printConsoleUsage();
            }

        }else if(command == "quit"){
            // Cancel each thread and exit
            for(size_t i = 0; i < connectionManager.size(); i++){
                if(connectionManager[i] != nullptr){
                    pthread_t threadID = connectionManager[i]->threadID;
                    connectionManager[i]->terminate(log);
                    cerr << "Connection " << connectionManager[i]->getClientID() << " is closed\n";
                    delete connectionManager[i];
                    connectionManager[i] = nullptr;
                    pthread_cancel(threadID);
                }
            }
            exit(0);
            break;

        }else{
            printConsoleUsage();
        }
    }
    

    return NULL;
}

int main(int argc, char *argv[])
{
    signal(SIGPIPE, SIG_IGN);

    // initialize variable and global variable
    if(!validateCommandLine(argc, argv)){
        return 0;
    }
    
    if(!praseConfig(string(argv[argc-1]))) return 0;
    ofstream pidFile(config["config"]["pid"]);
    pidFile << (int)getpid() << endl;
    pidFile.close();
    
    
    log.open(getConfigValue("config", "log"));
    if(!readMimeFile(getConfigValue("config", "mime"))){
        cerr << "Cannot read mime file\n";
        return 0;
    }
    
    
    
    char server_name[256];
    int reuse_addr = 1;
    int master_socket_fd;
    struct addrinfo hints;
    struct addrinfo* res;
    pthread_t console_thread_id;
    

    gethostname(server_name, sizeof(server_name));

    memset(&hints,0,sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = 0;
    hints.ai_flags = AI_NUMERICSERV|AI_ADDRCONFIG;

    getaddrinfo(server_name, config["config"]["port"].c_str(), &hints, &res);
    master_socket_fd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    setsockopt(master_socket_fd, SOL_SOCKET, SO_REUSEADDR, (void*)(&reuse_addr), sizeof(int));
    bind(master_socket_fd, res->ai_addr, res->ai_addrlen);
    listen(master_socket_fd, 1);
    {
        /* the following will show you exactly where your server thinks it's running at */
        struct sockaddr_in server_addr;
        socklen_t server_len = (socklen_t)sizeof(server_addr);

        getsockname(master_socket_fd, (struct sockaddr *)(&server_addr), &server_len);
        fprintf(stderr,
                "[SERVER] Server listening at %s:%1d\n",
                inet_ntoa(server_addr.sin_addr),
                (int)htons((uint16_t)(server_addr.sin_port & 0x0ffff)));
        writeServerInfo(inet_ntoa(server_addr.sin_addr), (int)htons((uint16_t)(server_addr.sin_port & 0x0ffff)));
    }
    
    pthread_create(&console_thread_id, NULL, console, NULL);
    
    for (;;) {
        struct sockaddr_in cli_addr;
        unsigned int clilen = sizeof(cli_addr);
        int newsockfd;
        pthread_t thread_id;
        
        
        newsockfd = accept(master_socket_fd, (struct sockaddr *)(&cli_addr), &clilen);
    
        pthread_create(&thread_id, NULL, handle_connection, (void*)(unsigned long)newsockfd);
        
    }
    close(master_socket_fd);

    return 0;
}

/*
    Definition of Functions
*/
void printConsoleUsage(){
    cout << "Command not recognized.  Valid commands are:\n"
         << "\tinfo #\n"
         << "\tstatus\n"
         << "\tdial # percent\n"
         << "\tclose #\n"
         << "\tquit\n";
}

bool praseConfig(string path){

    ifstream in;
    in.open(path.c_str());
    
    if(!in.is_open()){
        cerr << "Config file could not be opened\n";
        return false; // file not exist
    } 

    // read the file and parse it
    string line;
    int lineNum = 0;
    string sectionName;
    bool firstSection = true;
    
    bool hasPort = false;
    bool hasLog = false;
    bool hasPid = false;
    bool hasRoot = false;
    bool hasMime = false;

    while(getline(in, line)){
        lineNum++;
        if(line.empty())
            continue; // empty line
        size_t found = line.find_first_not_of(WHILE_SPACE);
        if(found == string::npos || line[found] == ';')
            continue; // empty line or comment line

        // not comment line
        if(line.find(WHILE_SPACE) != string::npos){
            cerr << "Config malformed at line " << lineNum << ": Cannot have while space here\n";
            return false; // cannot have space
        } 

        string key, value;
        if(line[0] == '['){
            // section name line
            sectionName = line.substr(1, line.length()-2);
            if(firstSection && sectionName != "config"){
                cerr << "Config malformed at line " << lineNum << ": section name config spelled incorrectly\n";
                return false;
            }
            firstSection = false;
            continue;
        }else{
            // parse the key-value part
            vector<string> keyValue = splitString(line, '=');
            if(keyValue.size() != 2){
                cerr << "Config malformed at line " << lineNum << ": Must have two fields\n";
                return false; // must have two part
            } 
            key = keyValue[0];
            value = keyValue[1];
        }
        // create key value map
        if(key != "port" && key != "log" && key != "pid" && key != "root" &&
           key != "mime" && key != "B"   && key != "P"   && key != "MAXR" &&
           key != "DIAL"){
               cerr << "Config malformed at line " << lineNum << ": Spell incorrect\n";
               return false;
           }

        if(key == "port" && stoi(value) < 1024){
            cerr << "Config malformed at line " << lineNum << ": port has been reserved\n";
            return false;
        }
        if(key == "MAXR" && (stoi(value) < 1 || stoi(value) > 1000)){
            cerr << "Config malformed at line " << lineNum << ": MAXR must be >= 1 and =< 1000\n";
            return false;
        }
        if(key == "B" && (stoi(value) < 1 || stoi(value) > 10)){
            cerr << "Config malformed at line " << lineNum << ": B must be >= 1 and =< 10\n";
            return false;
        }
        if(key == "DIAL" && (stoi(value) < 1 || stoi(value) > 100)){
            cerr << "Config malformed at line " << lineNum << ": DIAL must be >= 1 and =< 100\n";
            return false;
        }
        config[sectionName].insert(make_pair(key, value));
        if(config[sectionName].find("B") != config[sectionName].end() &&
           config[sectionName].find("P") != config[sectionName].end()){
               if(stoi(config[sectionName]["B"]) < stoi(config[sectionName]["P"])){
                    cerr << "Config malformed at line " << lineNum << ": B should be larger than P\n";
                    return false;
               }
               
        }

        if(key == "port") hasPort = true;
        else if(key == "log") hasLog = true;
        else if(key == "pid") hasPid = true;
        else if(key == "root")hasRoot = true;
        else if(key == "mime")hasMime = true;
    }

    if(!hasPort){
        cerr << "Config malformed: In config section, missing 'port'\n";
        return false;
    }else if(!hasLog){
        cerr << "Config malformed: In config section, missing 'log'\n";
        return false;
    }else if(!hasPid){
        cerr << "Config malformed: In config section, missing 'pid'\n";
        return false;
    }else if(!hasRoot){
        cerr << "Config malformed: In config section, missing 'root'\n";
        return false;
    }else if(!hasMime){
        cerr << "Config malformed: In config section, missing 'mime'\n";
        return false;
    }
    

    return true;
}

bool isDigit(string str, int& ret){
    stringstream ss(str);
    if(ss >> ret){
        return true;
    }else{
        return false;
    }
}

void writeServerInfo(string addr, int port){
    log << "Server listening at " << addr << ":" << port<< endl;
    root = getConfigValue("config", "root");
    
    if(root[0] != '/'){
        // not absolute path
        if(root.substr(0, 2) == "./"){
            root = root.substr(2);
        }
        char tempBuf[80];
        root = string(getcwd(tempBuf, sizeof(tempBuf))) + "/" + root;
    }
    
    log << "Server root at '" << root << "'\n";
    log << endl;
    log.flush();
}

string getConfigValue(string section, string key){
    string result = "";
    if(config.find(section) != config.end()){
        if(config[section].find(key) != config[section].end()){
            result = config[section][key];
        }else{
            cerr << "Error in 'getConfigValue': Cannot find key '" << key << "'\n";
        }
    }else{
        cerr << "Error in 'getConfigValue': Cannot find section '" << section << "'\n";
    }
    return result;
}

bool readMimeFile(string mimeFilePath){
    if(mimeFilePath[0] != '/'){
        char buf[80];
        mimeFilePath = string(getcwd(buf, sizeof(buf))) + "/" + mimeFilePath;
    }
    
    ifstream mimeFile(mimeFilePath);
    //ifstream mimeFile(filePathRoot + mimeFilePath);
    if(!mimeFile) return false; // mime file doesn't exist
    
    // read the file and deal with each line
    string line;
    int lineNum = 0;
    while(getline(mimeFile, line)){
        lineNum++;
        if(line.empty())
            continue; // empty line
        // find the first non space (tab) character
        size_t found = line.find_first_not_of(WHILE_SPACE);
        if(found == string::npos || line[found] == '#')
            continue; // empty line or comment line
        
        stringstream ss(line); // remove leading and trailing space
        string firstField, secondField;
        ss >> firstField >> secondField;
        
        /*
         * type=text/html exts=htm,html
         * firstField = text/html;
         * secondField = htm,html
         */
        // check the first field
        vector<string> firstFieldVec = splitString(firstField, '=');
        if(firstFieldVec.size() != 2){
            cerr << "Mime file malformed at line " << lineNum 
                 << ": The first field should be 'type=type/subtype'\n";
            return false;
        }
        string type = splitString(firstField, '=')[1]; //text/html
        if(splitString(firstField, '=')[0] != "type"){
            cerr << "Mime file malformed at line " << lineNum 
                 << ": The first field should be 'type=type/subtype'\n";
            return false;
        }
        if(type.find("/") == string::npos){
            cerr << "Mime file malformed at line " << lineNum
                 << ": The first filed should be 'type=type/subtype'\n";
            return false;
        }
        vector<string> fileExtVec = splitString(secondField, '=');
        if(fileExtVec.size() != 2){
            cerr << "Mime file malformed at line " << lineNum
                 << ": The file extension should not be empty\n";
            return false;
        }
        if(fileExtVec[1].find_first_not_of(WHILE_SPACE) == string::npos){
            cerr << "Mime file malformed at line " << lineNum
                 << ": The file extension should not be empty\n";
            return false;
        }

        vector<string> fileExt = splitString(splitString(secondField, '=')[1], ',');
        for(string ext: fileExt){
            if(mimeMap.find(ext) == mimeMap.end()){
                mimeMap.insert(make_pair(ext, type));
            }else{
                cerr <<"Mime file malformed at line " << lineNum <<
                 ": One file extension cannot map to two type\n";
                return false;
            }
            
        }
    }
    
    return true;
}

bool validateCommandLine(int argc, char *argv[]){
    if(argc < 2){
        cerr << "Command line malformed: too few arguments\n";
        
        return false;
    }
    if(argc > 2){
        cerr << "Command line malformed: too many arguments\n";
        return false;
        
    }
    if(string(argv[1])[0] == '-'){
        cerr << "Command line malformed\n";
        return false;
    }
    // check optional field
    for(int i = 1; i < argc - 1; i++){
        string field = string(argv[i]);
        if(field[0] != '-'){
            cerr << "Command line malformed: optional argument must have '-'\n";
            cerr << "Usage: pa2 [-port=number] [-log=logfilename] [-pid=pidfilename] [-root=directory] mimetypesfile\n";
            return false;
        }
        
        if(field.substr(1, 4) == "port" || field.substr(1, 3) == "log" ||
           field.substr(1, 3) == "pid" || field.substr(1, 4) == "root"){
            
        }else{
            cout << field.substr(1, 4) << endl;
            cerr << "Command line malformed: invalid optional argument\n";
            cerr << "Usage: pa2 [-port=number] [-log=logfilename] [-pid=pidfilename] [-root=directory] mimetypesfile\n";
            return false;
        }

        vector<string> split = splitString(field, '=');
        if(split.size() != 2){
            cerr << "Command line malformed: equal sign no content\n";
            cerr << "Usage: pa2 [-port=number] [-log=logfilename] [-pid=pidfilename] [-root=directory] mimetypesfile\n";
            return false;
        }
        size_t found = split[1].find_first_not_of("\t ");
        if(found == string::npos){
            cerr << "Command line malformed: equal sign no content\n";
            cerr << "Usage: pa2 [-port=number] [-log=logfilename] [-pid=pidfilename] [-root=directory] mimetypesfile\n";
            return false;
        }
        if(field.substr(1, 4) == "port" && 
            split[1].find_first_not_of("0123456789") != std::string::npos){
            cerr << "Command line malformed: port must be all digits\n";
            cerr << "Usage: pa2 [-port=number] [-log=logfilename] [-pid=pidfilename] [-root=directory] mimetypesfile\n";
            return false;
        }

    }
    return true;
}