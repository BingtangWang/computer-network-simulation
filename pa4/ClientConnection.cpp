#include "ClientConnection.h"
#include <iostream>
#include <ctime>
#include <sys/time.h>
#include <thread>
#include <chrono>
#include <fstream>
#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <stdio.h>
#include <unistd.h>
#include <algorithm>
#include <sstream>
#include <climits>
#include <openssl/md5.h>
#include <vector>
#include <string>
#include <iomanip>


struct Bucket{
    timeval t1;
    float b1;
};

double difftime(timeval& start, timeval& end){
    return end.tv_sec - start.tv_sec + (double)(end.tv_usec - start.tv_usec)/1000000;
}
/* Begin code (derived) from [https://stackoverflow.com/questions/236129/the-most-elegant-way-to-iterate-the-words-of-a-string] */
/* If the code you got requires you to include its copyright information, put copyright here. */
vector<string> splitstring(string str, char delimiter){
    std::vector<std::string> tokens;
    std::size_t start = 0, end = 0;
    while ((end = str.find(delimiter, start)) != std::string::npos) {
        if (end != start) {
            tokens.push_back(str.substr(start, end - start));
        }
        start = end + 1;
    }
    if (end != start) {
        tokens.push_back(str.substr(start));
    }
    return tokens;
}
/* End code from [https://stackoverflow.com/questions/236129/the-most-elegant-way-to-iterate-the-words-of-a-string] */

string timeStamp(){
    timeval tim;
    gettimeofday(&tim, NULL);
    
    string time = ctime(&tim.tv_sec);
    time = time.substr(0, time.length()-1);
    
    return "[Date: " + time.substr(0, 11) + time.substr(20, 4) + 
           " " + time.substr(11, 8) + "." + to_string(tim.tv_usec) + "] ";
}


ClientConnection::ClientConnection(void * arg, 
                                   int clientID, 
                                   std::unordered_map<string, std::unordered_map<string, string>> configMap,
                                   unordered_map<string, string> mimeMap,
                                   string root){
    this->_socketfd = (int)(unsigned long)arg;
    this->_configMap = configMap;
    this->_clientID = clientID;
    this->_mimeMap = mimeMap;
    this->_root = root + "/";
    string startTime = timeStamp();
    this->_startTime = startTime.substr(7, startTime.length()-9);
    gettimeofday(&this->_startTimeVal, NULL);
    this->threadID = pthread_self();
    _socketfd = (int)(unsigned long)arg;
    struct sockaddr_in peer;
    socklen_t peer_addr_len = (socklen_t)sizeof(peer);
    getpeername(_socketfd, (struct sockaddr *)(&peer), &peer_addr_len);
    _clientIP = inet_ntoa(peer.sin_addr);
    _clientPort = (int)htons((uint16_t)(peer.sin_port & 0x0ffff));
    read(_socketfd, _buf, sizeof(_buf));
}



void ClientConnection::response(ofstream& log){
    // determine the content of response
    //gettimeofday(&this->_startTimeVal, NULL);
    if(this->errorCode == 200){
        string line1 = "HTTP/1.1 200 OK\r\n";
        string line2 = "Connection: close\r\n";
        string line3 = "Server: pa2 bingtanw@usc.edu\r\n";
        string line4 = "Content-Type: " + this->fileType + "\r\n";
        string line5 = "Content-Length: " + to_string(this->length) + "\r\n";
        string line6 = "Content-MD5: " + this->fileMD5 + "\r\n";
        string line7 = "\r\n";
        string header =  line1 + line2 + line3 + line4 + line5 + line6 + line7;
        write(_socketfd, header.c_str(), header.length());
        
        

        // Assume have already parse the http request header and install filter
        if(_configMap.find(this->fileType) != _configMap.end()){
            const int bucketDepthB = stoi(_configMap[this->fileType]["B"]);
            const int tokenP = stoi(_configMap[this->fileType]["P"]);
            const int maxR = stoi(_configMap[this->fileType]["MAXR"]);
            _dial = stoi(_configMap[this->fileType]["DIAL"]);
             _sentBytes = 0;
            Bucket bucket;
            bucket.b1 = tokenP;
            gettimeofday(&bucket.t1, NULL);

            while(_sentBytes < this->length){
                timeval currentTime;
                gettimeofday(&currentTime, NULL);
                // calculate n: number of token arrived since t1
                float tokenRate = ((float)_dial / 100) * maxR;
                float n = difftime(bucket.t1, currentTime) * tokenRate;

                gettimeofday(&bucket.t1, NULL);
                // case 3: b1 + n >= P
                bucket.b1 += n;
                if(bucket.b1 >= tokenP){
                    bucket.b1 -= tokenP; // P has been removed
                    sendKBDataStartFrom(_sentBytes, log);
                    _sentBytes += 1024;
                }
                // case 2: b1 + n < P
                else if(bucket.b1 < tokenP){
                    float secondToSleep = (float)(tokenP - bucket.b1) / tokenRate;
                    this_thread::sleep_for(chrono::milliseconds((int)(secondToSleep * 1000)));
                    
                }
                // case 1: b1 + n >= B
                else if(bucket.b1 >= bucketDepthB){
                    bucket.b1 = bucketDepthB - tokenP;
                    sendKBDataStartFrom(_sentBytes, log);
                    _sentBytes += 1024;
                }
            }
        }else{
            while(_sentBytes < this->length){
                sendKBDataStartFrom(_sentBytes, log);
                _sentBytes += 1024;
            }
        }
        _sentBytes = this->length;

        

    }
    else if(this->errorCode == 400){
        string first = "HTTP/1.1 400 Bad Request\r\n";
        string second = "Connection: close\r\n";
        string third = "Content-Type: text/html\r\n";
        string fourth = "Content-Length: 65\r\n";
        string fifth = "\r\n";
        string sixth = "<html><head></head><body><h1>400 Bad Request</h1></body></html>\r\n";
        string res =  first + second + third + fourth + fifth + sixth;
        write(_socketfd, res.c_str(), res.length());
    }
    else if(this->errorCode == 404){
        string first = "HTTP/1.1 404 Not Found\r\n";
        string second = "Connection: close\r\n";
        string third = "Content-Type: text/html\r\n";
        string fourth = "Content-Length: 63\r\n";
        string fifth = "\r\n";
        string sixth = "<html><head></head><body><h1>404 Not Found</h1></body></html>\r\n";
        string res =  first + second + third + fourth + fifth + sixth;
        write(_socketfd, res.c_str(), res.length());
    }
    else if(this->errorCode == 405){
        string first = "HTTP/1.1 405 Method Not Allowed\r\n";
        string second = "Connection: close\r\n";
        string third = "Content-Type: text/html\r\n";
        string fourth = "Content-Length: 72\r\n";
        string fifth = "\r\n";
        string sixth = "<html><head></head><body><h1>405 Method Not Allowed</h1></body></html>\r\n";
        string res =  first + second + third + fourth + fifth + sixth;
        write(_socketfd, res.c_str(), res.length());
    }

    log << "\t[" << _clientID << "] "<< timeStamp() << "(DONE)\n";
    log.flush();
    
    
    close(_socketfd);
    
}

void ClientConnection::writeInLog(ofstream& log){
    // TODO: write into log after sending all data or terminated by user
    log << "[" << _clientID << "] Client connecting from " << _clientIP << ":" << _clientPort << endl;
    string httpheader = string(_buf);
    log << "\t[" << _clientID << "] ";
    for(size_t i = 0; i < httpheader.length(); i++){
        log << httpheader[i];
        if(httpheader[i] == '\n' && i != httpheader.length()-1){
            log << "\t[" << _clientID << "] ";
        }
    }
    log.flush();
    
    // parse URL and find file


    log << parseURL(log);

    // write sharper if installed
    if(_configMap.find(this->fileType) != _configMap.end()){
        int bucketDepthB = stoi(_configMap[this->fileType]["B"]);
        int tokenP = stoi(_configMap[this->fileType]["P"]);
        int maxR = stoi(_configMap[this->fileType]["MAXR"]);
        log << "\t[" << _clientID << "] " << timeStamp() << 
        "Shaper-Params: B=" << bucketDepthB << ", P=" << tokenP << ", MAXR=" << maxR << "/s, DIAL="
                            << stoi(_configMap[this->fileType]["DIAL"]) << "%, r=" << double(stoi(_configMap[this->fileType]["DIAL"]))/100 * maxR << " KB/s\n";
    }

    log.flush();
}

string ClientConnection::getTypeByExt(string ext){
    transform(ext.begin(), ext.end(), ext.begin(), ::tolower);
    unordered_map<string, string>::iterator it = _mimeMap.find(ext);
    if(it == _mimeMap.end()){
        return ""; // extension not found
    } 
    return it->second;
}

string ClientConnection::parseURL(ofstream& logFile){
    int i = 0; 
    string request = "";
    while(_buf[i] != '\n'){
        request += _buf[i++];
    }
    // GET /image.html?off=x&len=y HTTP/1.0\r\n
    stringstream ss(request);
    string method; // request method
    string URL; // request file path
    ss >> method >> URL;
    if(method != "GET"){
        this->errorCode = 405;
        return Error405();
    } 
    if(URL.find("/../") != string::npos){
        this->errorCode = 404;
        return Error404();
    }

    string x = "", y = "";
    // image.html?off=x&len=y
    vector<string> questionMark = splitstring(URL, '?');
    if(questionMark.size() >= 2){
        // parse off and len
        vector<string> andSign = splitstring(questionMark[questionMark.size()-1], '&');
        for(size_t i = 0; i < andSign.size(); i++){
            string field = andSign[i];
            if(field.substr(0,3) == "off"){
                vector<string> offset = splitstring(andSign[i], '=');
                if(offset.size() != 2){
                    this->errorCode = 400;
                    return Error400();
                }
                x = offset[1];
            }else if(field.substr(0,3) == "len"){
                vector<string> offset = splitstring(andSign[i], '=');
                if(offset.size() != 2){
                    this->errorCode = 400;
                    return Error400();
                }
                y = offset[1];
            }else{
                this->errorCode = 400;
                return Error400();
            }
        }
    }

    int error = 200;
    if(x.find_first_not_of("0123456789") != std::string::npos ||
       y.find_first_not_of("0123456789") != std::string::npos){
           this->errorCode = 400;
            return Error400();
    }

    string content = processFileWithOffset(questionMark[0], x, y, error, logFile); //TODO
    this->errorCode = error;
    if(error == 404) return Error404();
    if(error == 405) return Error405();
    if(error == 400) return Error400();

    return content;
}

string ClientConnection::Error405(){
    return "\t["+to_string(_clientID)+"] (ERROR) 405 Method Not Allowed\n" + 
           "\t["+to_string(_clientID)+"] " + timeStamp() + "(DONE)\n";
}
string ClientConnection::Error404(){
    return "\t["+to_string(_clientID)+"] (ERROR) 404 Not Found\n" + 
           "\t["+to_string(_clientID)+"] " + timeStamp() + "(DONE)\n";
}
string ClientConnection::Error400(){
    return "\t["+to_string(_clientID)+"] (ERROR) 400 Bad Request\n" + 
           "\t["+to_string(_clientID)+"] " + timeStamp() + "(DONE)\n";
}

string ClientConnection::processFileWithOffset(string filePath, string offset, string length, int& error, ofstream& logfile){
    
    
    long long fileLength = 0;
    string requestFile = "";
    string requestOff = "";
    string requestLen = "";
    string filePath2 = this->_root + filePath; //shift the path to default directory
    

    // get the file name
    vector<string> request = splitstring(filePath2, '/');
    requestFile = request[request.size() - 1];
    // get file type
    vector<string> comma = splitstring(requestFile, '.');
    string fileExt = comma[comma.size() - 1];
    this->fileType = getTypeByExt(fileExt);
    if(this->fileType == "") this->fileType = "application/octet-stream";

    requestFile = "\t["+to_string(_clientID)+"] Request file: '" + filePath.substr(1) + "'\n";
    if(offset != ""){
        requestOff = "\t["+to_string(_clientID)+"] Request off: " + offset + "\n";
    }
    if(length != ""){
        requestLen = "\t["+to_string(_clientID)+"] Request len: " + length + "\n";
    }
    
    ifstream file;
    file.open(filePath2, ios::in);
    
    logfile << requestFile << requestOff << requestLen;
    logfile.flush();
    // check error
    if(!file){ // 404 not found
        error = 404;
        return "";
    }
    fileLength = getFileSize(file);

    int off = atoi(offset.c_str());
    int len;
    if(off > fileLength){ // 404 not found
        error = 404;
        return "";
    }  
    
    if(length == "") len = fileLength;
    else len = atoi(length.c_str());
    
    if(off + len > fileLength) len = fileLength - off;

    // if(off != 0){
    //     this->fileType = "application/octet-stream";
    // }
    // else if(len != fileLength){
    //     this->fileType = "application/octet-stream";
    // }
    if(len != fileLength){
        cout << fileExt << endl;
        this->fileType = "application/octet-stream";
    }

    this->length = len;
    // get file content
    file.seekg(off, ios::beg);
    this->fileContent = new char[len];
    file.read(this->fileContent, len);

    file.close();
    
    // get file MD5
    unsigned char c[MD5_DIGEST_LENGTH];
    MD5_CTX mdContext;
    FILE *inFile = fopen(filePath2.c_str(), "rb");
    
    fseek(inFile, off, SEEK_SET);
    
    
    int bytes;
    unsigned char* data = new unsigned char[len];
    MD5_Init(&mdContext);
    
    if ((bytes = fread (data, 1, len, inFile)) != 0)
        MD5_Update (&mdContext, data, bytes);
    MD5_Final (c,&mdContext);
    fclose (inFile);
    delete data;

    stringstream s;
    for(int i = 0; i < 16; ++i){
        s << std::hex << std::setfill('0') << std::setw(2) << (unsigned short)c[i];
    }
    this->fileMD5 = s.str();
    

    
    string fileTypeTemp = "\t["+to_string(_clientID)+"] Content-Type: " + this->fileType + "\n";
    string contentLength = "\t["+to_string(_clientID)+"] Content-Length: " + to_string(len) + "\n";
    string fileMD5Temp = "\t["+to_string(_clientID)+"] Content-MD5: " + this->fileMD5 + "\n";
    
    
    string result = fileTypeTemp + contentLength + fileMD5Temp; 
    file.close();
    return result;
}

long long ClientConnection::getFileSize(ifstream& file){
    streampos fsize = file.tellg();
    file.seekg(0, ios::end);
    fsize = file.tellg() - fsize;
    return fsize;
}

void ClientConnection::sendKBDataStartFrom(long long start, ofstream& log){
    char buf[1];
    for(long long i = start; i < (start + 1024) && i < this->length; i++){
        buf[0] = fileContent[i];
        if(write(_socketfd, buf, 1) == -1){
            if(errno == EPIPE){
                terminate(log);
            }   
        }
        
    }
}

void ClientConnection::printInfo(){
    {
        lock_guard<mutex> guard(_mutex);
        cerr << "[" << _clientID << "] Client connecting from " << _clientIP << ":" << _clientPort << endl;
        cerr << "\tContent-Type: " << this->fileType << "\n";
        cerr << "\tContent-Length: " << this->length << "\n";
        cerr << "\tContent-MD5: " << this->fileMD5 << "\n";
        cerr << "\tStart-Time: " << this->_startTime << endl;
        if(_configMap.find(this->fileType) != _configMap.end()){
            int bucketDepthB = stoi(_configMap[this->fileType]["B"]);
            int tokenP = stoi(_configMap[this->fileType]["P"]);
            int maxR = stoi(_configMap[this->fileType]["MAXR"]);
            cerr << "\tShaper-Params: B=" << bucketDepthB 
                << ", P=" << tokenP 
                << ", MAXR=" << maxR 
                << ", DIAL=" << _dial << "%"
                << ", r=" << (double)_dial/100 * maxR << " KB/s\n";
        }
        timeval currentTime;
        gettimeofday(&currentTime, NULL);
        double timePass = difftime(this->_startTimeVal, currentTime);
        cerr << "\tSent: " << _sentBytes/1024 << "KB of " << this->length/1024 
            << "KB (" << (double)_sentBytes/this->length * 100 << "%) downloaded at "
            << _sentBytes/timePass/1000 << " KB/s, time elapsed: " << timePass << "s\n";
    }
}

void ClientConnection::shutdown(){
    close(_socketfd);
    
}

void ClientConnection::terminate(ofstream& log){
    close(_socketfd);
    if(this->_sentBytes != this->length)
        log << "\t[" << _clientID << "] " << timeStamp() << "(TERMINATED BY USER)\n";
    log.flush();
    hasTerminated = true;
}

double ClientConnection::getTokenRate(){
    int maxR = stoi(_configMap[this->fileType]["MAXR"]);
    return (double)_dial/100 * maxR;
}

void ClientConnection::setDial(int dial, ofstream& log){
    _dial = dial;
    if(_configMap.find(this->fileType) != _configMap.end()){
        int bucketDepthB = stoi(_configMap[this->fileType]["B"]);
        int tokenP = stoi(_configMap[this->fileType]["P"]);
        int maxR = stoi(_configMap[this->fileType]["MAXR"]);
        log << "\t[" << _clientID << "] " << timeStamp() << 
        "Shaper-Params: B=" << bucketDepthB << ", P=" << tokenP << ", MAXR=" << maxR << "/s, DIAL="
                            << _dial << "%, r=" << double(_dial)/100 * maxR << " KB/s\n";
    }
    log.flush();    
}
    