#include <iostream>
#include <unordered_map>
#include <vector>
#include <fstream>
#include <memory>
#include <mutex>

using namespace std;

class ClientConnection{
public:
    ClientConnection(void *arg, 
                     int clientID, 
                     unordered_map<string, unordered_map<string, string>> configMap, 
                     unordered_map<string, string> mimeMap,
                     string root);
    void response(ofstream& log);
    void writeInLog(ofstream& log);
    void printInfo();

    //getter and setter
    int getClientID(){return _clientID;}
    void setDial(int dial, ofstream& log);
    int getDial(){return _dial;}
    double getTokenRate();
    string getStartTime(){return _startTime;}


    string getTypeByExt(string ext);
    string parseURL(ofstream& logFile);
    string processFileWithOffset(string filePath, string offset, string len, int& error, ofstream& logfile);
    long long getFileSize(ifstream& file);
    void sendKBDataStartFrom(long long start, ofstream& log);
    string Error405();
    string Error404();
    string Error400();
    void shutdown();
    void terminate(ofstream& log);

    pthread_t threadID;
    bool hasTerminated = false;
private:
    int _clientID;
    int _socketfd;
    int _dial;
    char _buf[1024]; // 1KB data for reading and writing data
    unordered_map<string, unordered_map<string, string>> _configMap;
    unordered_map<string, string> _mimeMap;
    string _clientIP;
    int _clientPort;
    string _root;
    string _startTime;
    long long _sentBytes;
    timeval _startTimeVal;

    char* fileContent;

    string fileType;
    long long length;
    string fileMD5;
    int errorCode = 200;
    
    
    // status variable
    mutex _mutex;
    


};