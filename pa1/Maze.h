//
//  Maze.hpp
//  PA1
//
//  Created by WangBingtang on 1/12/18.
//  Copyright © 2018 WangBingtang. All rights reserved.
//

#ifndef Maze_h
#define Maze_h
#include <vector>
#include <iostream>



using namespace std;

/*
    Header of Cell class
 */
class Cell {
public:
    int x, y;
    vector<Cell*> adjacency;
    vector<int> edges;
    int val;
    Cell(int x, int y);
    bool operator == (const Cell* other);
    void addAdjacent(Cell* other){ adjacency.push_back(other);}
    void addEdges(int edge){ edges.push_back(edge);}
};

/*
    Header of Maze class
 */
class Maze {
private:
    string file;
    int row, col;
    vector<vector<char>> maze;
    vector<vector<Cell*>> cells;
    
public:
    Maze(string file); // constructor with file name
    ~Maze();
    
    string validate(); // validate the maze file and store the file content into raw matrix
    void parse(); // parse the file into maze, if file content has error, return false;
    void print(); // print the maze;
    void reset();
    int findEdgeCost(Cell* cell1, Cell* cell2);
    int findEdgeCost(int i1, int j1, int i2, int j2);
    
    bool Dijkstra(int i, int j); // dfs starting with i , j
    void output();
    
};

/*
    Header of PQCell
 */
struct PQCellCompare {
    bool operator()(const Cell* lhs, const Cell* rhs) {
        return lhs->val > rhs->val;
    }
};



#endif /* Maze_h */
