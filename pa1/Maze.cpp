//
//  Maze.cpp
//  PA1
//
//  Created by WangBingtang on 1/12/18.
//  Copyright © 2018 WangBingtang. All rights reserved.
//

#include "Maze.h"
#include <fstream>
#include <iostream>
#include <queue>
#include <climits>

using namespace std;

const string LINE_SIZE_NOT_EQUAL = "Line size not equal. ";
const string MAZE_FORMAT_ERROR = "Maze format error. ";

bool isDigit(char c){
    return (c >= '0' && c <= '9');
}
string errorAtLine(int x){
    return "Error in Line " + to_string(x) + ". ";
}

/*
    Definition of Cell class
 */

Cell::Cell(int x, int y){
    this->x = x;
    this->y = y;
    this->val = INT_MAX;
}

bool Cell::operator==(const Cell* other){
    return (this->x == other->x && this->y == other->y);
}

/*
    Definition of Maze class
 */

Maze::Maze(string file){
    this->file = file;
    row = 0; col = 0;
}

Maze::~Maze(){
    for(int i = 0; i < cells.size(); i++){
        for(int j = 0; j < cells[i].size(); j++){
            delete cells[i][j];
        }
    }
}


/*
 -1: everything fine
 -2: file not found
 -3: empty file
 -4: col size too large
 -5: row size too large
 postive interger x: error one line x
 */

string Maze::validate(){
    ifstream in(this->file);
    if(!in) return "File not found";
    
    // read the file and store into raw vector
    string line;
    while(getline(in, line)){
        if(line.empty()) continue;
        vector<char> rowVec;
        for(char c: line){
            rowVec.push_back(c);
        }
        this->maze.push_back(rowVec);
    }
    
    // validate the length of each line
    if(maze.size() == 0 || maze[0].size() == 0) return "Empty file";
    unsigned long colLength = maze[0].size();
    unsigned long rowLength = maze.size();
    for(int i = 1; i < maze.size(); i++){
        if(maze[i].size() != colLength){
            if(i == maze.size() - 1){
                // the last line
                return LINE_SIZE_NOT_EQUAL + errorAtLine(i+1);
            }else{
                if(maze[i+1].size() == maze[i].size())
                    return LINE_SIZE_NOT_EQUAL + errorAtLine(i);
            }
            return LINE_SIZE_NOT_EQUAL + errorAtLine(i+1);
        }
    }
    if(colLength > 130) return "column size too large";
    if(rowLength > 66) return "row size too large";
    
    // validate the border
    for(int i = 0; i < maze.size(); i++){
        for(int j = 0; j < maze[i].size(); j++){
            if(i == 0 || i == maze.size() - 1){
                // first line and the last line
                if(j % 2 == 0 && maze[i][j] != '+')
                    return MAZE_FORMAT_ERROR + "Boundary format malformed. "
                           + errorAtLine(i+1);
                if(j % 2 == 1 && maze[i][j] != '-')
                    return MAZE_FORMAT_ERROR + "Boundary format malformed. "
                           + errorAtLine(i+1);
            }
            if(j == 0 || j == colLength - 1){
                if(i % 2 == 0 && maze[i][j] != '+')
                    return MAZE_FORMAT_ERROR + "Boundary format malformed. "
                           + errorAtLine(i+1);
                if(i % 2 == 1 && maze[i][j] != '|')
                    return MAZE_FORMAT_ERROR + "Boundary format malformed. "
                           + errorAtLine(i+1);
            }
        }
    }
    
    // validate the format of '+'
    for(int i = 0; i < maze.size(); i++){
        for(int j = 0; j < maze[i].size(); j++){
            if(i % 2 == 0 && j % 2 == 0){
                if(maze[i][j] != '+')
                    return MAZE_FORMAT_ERROR + "Should be '+'. "
                           + errorAtLine(i+1);
            }
        }
    }
    // validate the format of '|'
    for(int i = 1; i < maze.size(); i+=2){
        for(int j = 0; j < maze[i].size(); j+=2){
            if(maze[i][j] != '|' && maze[i][j] != ' ' &&
               !(maze[i][j] >= '0' && maze[i][j] <= '9'))
                return MAZE_FORMAT_ERROR + "Should be '|' or ' '. "
                           + errorAtLine(i+1);
        }
    }
    // validate the format of door way
    bool typeOneMaze = false, check = false;
    for(int i = 1; i < maze.size(); i+=2){
        for(int j = 2; j < maze[i].size(); j+=2){
            if(maze[i][j] == '|') continue;
            if(!check && maze[i][j] == ' '){
                typeOneMaze = true;
                check = true;
            }else if(!check && isDigit(maze[i][j])){
                typeOneMaze = false;
                check = true;
            }
            
            if(check && typeOneMaze && maze[i][j] != ' ')
                return MAZE_FORMAT_ERROR + "Should be ' ' in type I maze. "
                           + errorAtLine(i+1);
            if(check && !typeOneMaze && !isDigit(maze[i][j]))
                return MAZE_FORMAT_ERROR + "Should be number in type II maze. "
                           + errorAtLine(i+1);
        }
    }
    typeOneMaze = false; check = false;
    for(int i = 2; i < maze.size(); i+=2){
        for(int j = 1; j < maze[i].size(); j+=2){
            if(maze[i][j] == '-') continue;
            if(!check && maze[i][j] == ' '){
                typeOneMaze = true;
                check = true;
            }else if(!check && isDigit(maze[i][j])){
                typeOneMaze = false;
                check = true;
            }
            
            if(check && typeOneMaze && maze[i][j] != ' ')
                return MAZE_FORMAT_ERROR + "Should be ' ' in type I maze. "
                           + errorAtLine(i+1);
            if(check && !typeOneMaze && !isDigit(maze[i][j]))
                return MAZE_FORMAT_ERROR + "Should be number in type II maze. "
                           + errorAtLine(i+1);
        }
    }
    
    
    this->row = (int)maze.size() / 2;
    this->col = (int)maze[0].size() / 2;
    
    return "";
}

void Maze::reset(){
    for(int i = 0; i < cells.size(); i++){
        for(int j = 0; j < cells[i].size(); j++){
            cells[i][j]->val = INT_MAX;
        }
    }
}

void Maze::parse(){
    for(int i = 0; i < this->row; i++){
        vector<Cell*> vecRow;
        for(int j = 0; j < this->col; j++){
            Cell* cell = new Cell(i, j);
            vecRow.push_back(cell);
        }
        this->cells.push_back(vecRow);
    }
    
    // find adjacent node
    int adj[4][2]{{0, 1}, {1, 0}, {-1, 0}, {0, -1}};
    // each cell is (2i+1, 2j+1)
    for(int i = 0; i < this->row; i++){
        for(int j = 0; j < this->col; j++){
            for(int k = 0; k < 4; k++){
                int indexI = 2 * i + 1 + adj[k][0];
                int indexJ = 2 * j + 1 + adj[k][1];
                if(maze[indexI][indexJ] == ' '){
                    // two cells are connected
                    cells[i][j]->addAdjacent(cells[i + adj[k][0]][j + adj[k][1]]);
                    cells[i][j]->addEdges(1);
                }
                else if(maze[indexI][indexJ] >= '0' && maze[indexI][indexJ] <= '9'){
                    cells[i][j]->addAdjacent(cells[i + adj[k][0]][j + adj[k][1]]);
                    cells[i][j]->addEdges(maze[indexI][indexJ] - '0');
                }
                else{
                    continue;
                }
            }
        }
    }
    

}

void Maze::print(){
    for(int i = 0; i < maze.size(); i++){
        for(int j = 0; j < maze[0].size(); j++){
            if(i % 2 == 1 && j % 2 == 1 && cells[i/2][j/2]->val != INT_MAX){
                cout << cells[i/2][j/2]->val % 10;
            }else{
                cout << maze[i][j];
            }
        }
        cout << endl;
    }
}

int Maze::findEdgeCost(Cell* cell1, Cell* cell2){
    return findEdgeCost(cell1->x, cell1->y, cell2->x, cell2->y);
}

int Maze::findEdgeCost(int i1, int j1, int i2, int j2){
    Cell* cell = cells[i1][j1];
    for(int i = 0; i < cell->adjacency.size(); i++){
        if(cell->adjacency[i]->x == i2 && cell->adjacency[i]->y == j2){
            return cell->edges[i];
        }
    }
    return -1;
}

bool Maze::Dijkstra(int i, int j){
    if(i < 0 || j < 0 || i >= this->row || j >= this->col){
        return false;
    }
    if(maze.empty()) return false;
    
    priority_queue<Cell*, vector<Cell*>, PQCellCompare > PQCells;
    cells[i][j]->val = 0;
    Cell* start = cells[i][j];
    PQCells.push(start);
    while(!PQCells.empty()){
        Cell* curr = PQCells.top();
        PQCells.pop();
        for(int i = 0; i < curr->adjacency.size(); i++){
            int distance = curr->val + curr->edges[i];
            if(distance < curr->adjacency[i]->val){
                curr->adjacency[i]->val = distance;
                PQCells.push(curr->adjacency[i]);
            }
        }
    }
    return true;
}

void Maze::output(){
    ofstream out(file + "-solved.txt");
    for(int i = 0; i < maze.size(); i++){
        for(int j = 0; j < maze[0].size(); j++){
            if(i % 2 == 1 && j % 2 == 1 && cells[i/2][j/2]->val != INT_MAX){
                out << cells[i/2][j/2]->val % 10;
            }else{
                out << maze[i][j];
            }
        }
        out << endl;
    }
}

