Documentation for Warmup Assignment 1
=====================================

+------------------------+
| BUILD & RUN (Required) |
+------------------------+

Replace "(Comments?)" with either nunki.usc.edu, VMware, VirtualBox, or Vagrant Box:
    The grader should grade your submission on: VirtualBox
Replace "(Comments?)" with either Solaris (for nunki) or Ubuntu 14.04 or Ubuntu 16.04:
    I would prefer to be graded on: Ubuntu 16.04
If grading needs to be done on Ubuntu, replace "(Comments?)" with the names of the
packages your program depends on (to be used in "sudo apt-get install PACKAGENAME"):
    I need the following packages to be installed: tcsh
Replace "(Comments?)" with the command the grader should use to compile your code (could simply be "make" or "gmake").
    To compile your code, the grader should type: make

+--------------+
| SELF-GRADING |
+--------------+

Replace each "?" below with a numeric value:

(A) Type-I Maze, Batch Mode : 40  out of 40 pts
(B) Type-II Maze, Batch Mode : 40  out of 40 pts
(C) Type-I Maze, Interactive Mode : 40 out of 10 pts
(D) Type-II Maze, Interactive Mode : 10 out of 10 pts

Missing/incomplete required section(s) in README file : -0 pts
Submitted binary file : -0 pts
Cannot compile : -0 pts
Compiler warnings   : -0 pts
"make clean" : -0 pts
Segmentation faults : -0 pts
Program never terminates : -0 pts
Malformed input : -0 pts
Too slow : -0 pts
Bad commandline or command : -0 pts

+----------------------+
| BUGS / TESTS TO SKIP |
+----------------------+

Are there are any tests mentioned in the grading guidelines test suite that you
know that it's not working and you don't want the grader to run it at all so you
won't get extra deductions, please replace "(Comments?)" below with your list.
(Of course, if the grader won't run such tests in the plus points section, you
will not get plus points for them; if the garder won't run such tests in the
minus points section, you will lose all the points there.)  If there's nothing
the grader should skip, please replace "(Comments?)" with "none".

Please skip the following tests: (Comments?)

+-----------------------------------------------+
| OTHER (Optional) - Not considered for grading |
+-----------------------------------------------+

Comments on design decisions: (Comments?)

