//
//  main.cpp
//  PA1
//
//  Created by WangBingtang on 1/12/18.
//  Copyright © 2018 WangBingtang. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <sstream>
#include "Maze.h"

using namespace std;

/*
 Error cerr output
 */
void printNoFileInputError(){
    cerr << "Input Error: No file input\n";
}
void printOutOfRangeError(){
    cerr << "Input starting location out of range\n";
}
void printMazeUndefinedError(){
    cerr << "Cannot execute the 'spt' command.  Current maze is undefined." << endl;
}
void printMazeFormatError(){
    cerr << "Maze format error" << endl;
}
void printCommandNameError(string name){
    cerr << "Invalid command: '" + name + "'." << endl;
}
void printInputLineTooLongError(){
    cerr << "Input line too long." << endl;
}
void printUsageError(){
    cerr << "usage: pa1 [-root=r,c filename]" << endl;
}
void printExtraInputError(){
    cerr << "Input Error: Extra input\n";
}

/*
 Regular status output
 */
void printMazeSetMessage(string fileName){
    cout << "Current maze set to '" + fileName + "'." << endl;
}

void printShortestPathMessage(int x, int y){
    cout << "Shortest-path tree rooted at (" << x << "," << y << "):\n";
}

/*
    Interactive Mode
 */
void interactive(Maze* maze){
    string dummy; // dummy variable used for checking extra input
    while(true){
        string command;
        cout << "> ";
        getline(cin, command);
        if(command.length() > 260){
            // input too long
            printInputLineTooLongError();
            printUsageError();
            continue;
        }
        stringstream ss(command);
        string tag;
        ss >> tag;
        if(tag == "maze"){
            // operation with maze tag
            string fileName;
            if(ss >> fileName){
                if(ss >> dummy){
                    // has extra input
                    printExtraInputError();
                    printUsageError();
                    continue;
                }else{
                    // input ok, intialize maze
                    if(maze != nullptr) delete maze;
                    maze = new Maze(fileName);
                    // validate the maze
                    string error = maze->validate();
                    if(error.empty()){
                        // maze ok
                        printMazeSetMessage(fileName);
                        maze->parse();
                        maze->print();
                    }else{
                        cerr << error << endl;
                        delete maze;
                        maze = nullptr;
                        continue;
                    }
                }
            }else{
                // doesn't have file name input
                printNoFileInputError();
                printUsageError();
                continue;
            }
        }else if(tag == "spt"){
            // spt tag
            int x, y;
            ss >> x >> y;
            string dummy;
            if(ss >> dummy){
                // extra input
                printExtraInputError();
                printUsageError();
                continue;
            }
            if(maze == nullptr){
                // maze has not been intialized
                printMazeUndefinedError();
                continue;
            }
            maze->reset();
            if(maze->Dijkstra(x, y)){
                printShortestPathMessage(x, y);
                maze->print();
            }else{
                // fail to locate the starting point
                printOutOfRangeError();
                continue;
            }
        }else if(tag == "quit" || tag.empty()){
            break;
        }else{
            printCommandNameError(tag);
            printUsageError();
        }
    }
}

/*
 Batch mode
 */
void batch(Maze* maze, const char * argv[]){
    string command = argv[1];
    string fileName = argv[2];
    string tag = command.substr(0, 5);
    maze = new Maze(fileName);
    string error = maze->validate();
    if(!error.empty()){
        // has error
        cerr << error << endl;
        delete maze;
        maze = nullptr;
        return;
    }
    if(tag != "-root"){
        printCommandNameError(tag);
        printUsageError();
        return;
    }
    
    size_t comma = command.find(",");
    if(comma != string::npos){
        while(command[comma] != '='){
            comma--;
        }
        comma++;
        string details = "";
        while(comma < command.length()){
            if(command[comma] == ','){
                details += " ";
            }else{
                details += command[comma];
            }
            comma++;
        }
        
        stringstream ssDetail(details);
        int r = 0, c = 0;
        if(ssDetail >> r >> c){
            
        }else{
            // input error
            printCommandNameError(command);
            printUsageError();
            return;
        }
        printMazeSetMessage(fileName);
        maze->parse();
        maze->print();
        if(maze->Dijkstra(r, c)){
            printShortestPathMessage(r, c);
            maze->print();
        }else{
            // fail to locate the starting point
            printOutOfRangeError();
            return;
        }
        
    }else{
        printCommandNameError(command);
        printUsageError();
        return;
    }
}


int main(int argc, const char * argv[]) {
    
    Maze* maze = nullptr;
    if(argc < 2){
        // interactive mode
        interactive(maze);
    }else if(argc == 3){
        batch(maze, argv);
        
    }else{
        cerr << "Argument number incorrect.\n";
        printUsageError();
    }
    return 0;
}
